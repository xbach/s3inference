//enablePlugins(PackPlugin)

name := "s3repair"

version := "1.0"

scalaVersion := "2.12.6"

//javacOptions ++= Seq("-source", "1.8", "-target", "1.8")

//scalacOptions += "-target:jvm-1.8"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"
//resolvers += "Maven repo1" at "http://repo1.maven.org/"

//libraryDependencies += "com.typesafe.akka" % "akka-actor" % "2.0.2"

libraryDependencies ++= Seq("net.liftweb" %% "lift-json" % "3.3.0-M2",
  "org.scala-lang" % "scala-library" % "2.12.6",
  "org.eclipse.jdt" % "org.eclipse.jdt.core" % "3.10.0",
  "org.testng" % "testng" % "6.14.3",
  "log4j" % "log4j" % "1.2.17",
  "org.apache.commons" % "commons-math3" % "3.0",
  "org.apache.commons" % "commons-lang3" % "3.7",
  "commons-io" % "commons-io" % "2.6",
  "me.tongfei" % "progressbar" % "0.6.0",
  "jgrapht" % "jgrapht" % "0.7.3",
  "org.choco-solver" % "choco-solver" % "4.0.2"
  //"org.apache.maven.shared" % "maven-invoker" % "3.0.0"
)

// for debugging sbt problems
logLevel := Level.Debug

//scalacOptions += "-deprecation"

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}

//packMain := Map("hello" -> "driver.Main")

exportJars := true

//mainClass in (Compile, packageBin) := Some("driver.Main")

mainClass in assembly := Some("driver.Main")