package backend;

import driver.Options;
import gov.nasa.jpf.Config;
import gov.nasa.jpf.Error;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.PropertyListenerAdapter;
import gov.nasa.jpf.report.ConsolePublisher;
import gov.nasa.jpf.search.Search;
import gov.nasa.jpf.symbc.SymbolicInstructionFactory;

import gov.nasa.jpf.symbc.concolic.PCAnalyzer;
import gov.nasa.jpf.symbc.numeric.PCChoiceGenerator;
import gov.nasa.jpf.symbc.numeric.PathCondition;
import gov.nasa.jpf.symbc.numeric.SymbolicConstraintsGeneral;
import gov.nasa.jpf.vm.ChoiceGenerator;

import gov.nasa.jpf.vm.VM;
import org.apache.log4j.Logger;
import utils.FileFolderUtils;

import java.io.*;
import java.util.ArrayList;

public class MySymbolicListener extends PropertyListenerAdapter {
    private Logger logger = Logger.getLogger(this.getClass());
    private ArrayList<String> installedSymbLocs = null;

    public MySymbolicListener(Config conf, JPF jpf, ArrayList<String> symbcLocs) {
        jpf.addPublisherExtension(ConsolePublisher.class, this);
        installedSymbLocs = symbcLocs;
    }

    @Override
    public void searchConstraintHit(Search search) {
        if(Options.terminateSearchWhenHit()) {
            search.isDone();
            search.terminate();
        }
        logger.info("Search terminated due to search constraint hit...");
        // TODO: testing feature of search constraint hit
        /*System.out.print("HIT");
        System.out.print("HIT11");
        VM vm = search.getVM();
        ChoiceGenerator<?> cg = findPCChoiceGenerator(vm);
        processPC(cg, search);*/
    }


    @Override
    public void searchFinished(Search search){
        //System.out.print("XXX");
        //System.out.print("XXX11");
        //System.out.println("debug");
        if(!search.hasErrors()) {
            VM vm = search.getVM();
            ChoiceGenerator<?> cg = findPCChoiceGenerator(vm);
            if ((cg instanceof PCChoiceGenerator)
                    && ((PCChoiceGenerator) cg).getCurrentPC() != null) {
                PathCondition pc = ((PCChoiceGenerator) cg).getCurrentPC();
                String fileName = FileFolderUtils.getInstrumentedDirWithPrefix()+ File.separator + "solvedPC.txt";
                if(!new File(fileName).exists()) {
                    try {
                        CustomUnitTestCaller.writeSolvedPC(fileName, pc.toString());
                        logger.info("Wrote PC when search finished without error!");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public static ChoiceGenerator<?> findPCChoiceGenerator(VM vm) {
        ChoiceGenerator<?> cg = vm.getChoiceGenerator();
        if (!(cg instanceof PCChoiceGenerator)) {
            ChoiceGenerator<?> prev_cg = cg.getPreviousChoiceGenerator();
            while (!((prev_cg == null) || (prev_cg instanceof PCChoiceGenerator))) {
                prev_cg = prev_cg.getPreviousChoiceGenerator();
            }
            cg = prev_cg;
        }
        return cg;
    }


    @Override
    public void propertyViolated(Search search) {
        logger.info("Entering property violated...");
        VM vm = search.getVM();
        /*ChoiceGenerator cg = vm.getChoiceGenerator();
        if (!(cg instanceof PCChoiceGenerator)) {
            ChoiceGenerator pc;
            for (pc = cg.getPreviousChoiceGenerator(); pc != null && !(pc instanceof PCChoiceGenerator); pc = pc.getPreviousChoiceGenerator()) {
                ;
            }

            cg = pc;
        }*/
        ChoiceGenerator cg = findPCChoiceGenerator(vm);
        processPC(cg, search);
    }

    private void processPC(ChoiceGenerator cg, Search search){
        Error lastError = search.getLastError();
        // TODO: this is a testing feature of search constraint hit. Currently not enabled yet
        String error = lastError != null? lastError.getDetails(): "XNoErrorButSearchConstraintHitX";
        //error = "\"" + error.substring(0, error.indexOf("\n")) + "...\"";

        // If assertion error at this point, it means the pc does not lead to an assertion error free state
        // So we do not care about this pc. We only care only if it is not assertion error, but still some
        // errors happen when jpf running, e.g., index out of bound. In this case, just remove the last instance
        // in the pc, which causes the index out of bound.
        if(error.contains("java.lang.AssertionError")||error.contains("org.junit.ComparisonFailure") ||error.contains("AssertionFailedError"))
            return;

        if (cg instanceof PCChoiceGenerator && ((PCChoiceGenerator) cg).getCurrentPC() != null) {
            PathCondition pc1 = ((PCChoiceGenerator) cg).getCurrentPC();
            try {
                String fileName = FileFolderUtils.getInstrumentedDirWithPrefix()+ File.separator + "solvedPC.txt";
                // If exist path condition written (by CustomUnitTestCaller),
                // this means the written pc is the one satisfies the output constraint,
                // and is independent with the path here. So we do not write out the path here
                // because it does not satisfy the output constraint.
                // @See multiline test with independent paths
                // However, if the pc is not written before,
                // which means this pc is the only path so far, we write this path to file, and need to pop
                // out the latest instance and remove that instance, keep other instances.
                // @See for-loop test with array out of bound.
                if(!new File(fileName).exists()) {
                    if (SymbolicInstructionFactory.concolicMode) {
                        SymbolicConstraintsGeneral pcPair = new SymbolicConstraintsGeneral();
                        PCAnalyzer methodSummary = new PCAnalyzer();
                        methodSummary.solve(pc1, pcPair);
                    } else {
                        pc1.solve();
                    }
                    ParseSolvedPC parser = new ParseSolvedPC(false);
                    if(error.compareTo("NoErrorButSearchConstraintHit") != 0 && !error.contains("java.lang.RuntimeException: java.security.NoSuchAlgorithmException")) {
                            String removedLastInstance = parser.removeLastInstance(pc1.toString().split(System.getProperty("line.separator")));
                            if (removedLastInstance != null) {
                                CustomUnitTestCaller.writeSolvedPC(fileName, removedLastInstance + "\n");
                                logger.info("Wrote pc with property violated. Already removed latest instance");
                            } else {
                                logger.info("Dit not write pc with property violated because removed latest instance null.");
                            }
                    }else{
                        // handle the case of nosuchalgorithmexception bug in qabel-core_4. This is a hack, but correct.
                        CustomUnitTestCaller.writeSolvedPC(fileName, pc1.toString());
                        logger.info("Wrote pc with property violated.");
                    }

                }else{
                    logger.info("Dit not write pc with property violated because previous pc file exist.");
                }
                logger.info("Exit property violated...");
            } catch (IOException e) {
                e.printStackTrace();
            }

            logger.debug("Property Violated: PC is " + pc1.toString());
            logger.debug("Property Violated: result is  " + error);
            logger.debug("****************************");

        }
    }
}
