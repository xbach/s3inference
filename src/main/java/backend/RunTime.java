package backend;

import gov.nasa.jpf.symbc.Debug;
import sun.misc.FloatingDecimal;


import java.io.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * Created by dxble on 8/27/16.
 */

public class RunTime {
    public static String VAR_SEPARATOR = "_";

    public static boolean SYMBOLIC_RUNTIME = true;
    private static HashMap<String, Integer> outputInstances = null;
    private static HashMap<String, Integer> choiceInstances = null;
    private static boolean DEBUG = Boolean.parseBoolean(System.getProperty("DEBUG"));

    private static void printDebugInfo(String info){
        if(DEBUG){//DEBUG
            System.out.println(info);
        }
    }

    private static Boolean isSymbolicRuntime(){
        String isSbr = System.getProperty("SYMBOLIC_RUNTIME");
        return Boolean.parseBoolean(isSbr);
        //return true;
    }

    private static Boolean isDumpingOutput(){
        String isToDump = System.getProperty("ANGELIX_DUMP");
        printDebugInfo("**DUMPING ="+isToDump);
        return Boolean.parseBoolean(isToDump);
        //return true;
    }

    private static int outputInstanceOf(String id) {
        if(outputInstances == null)
            outputInstances = new HashMap<>();
        Integer i = outputInstances.getOrDefault(id, -1); //Java 8 feature

        if(i.intValue() == -1){
            outputInstances.put(id, 0);
            return 0;
        }else{
            Integer j = i + 1;
            outputInstances.put(id, j);
            return j.intValue();
        }
    }

    private static int choiceInstanceOf(int bl, int bc, int el, int ec) {
        String id = bl+"-"+bc+"-"+el+"-"+ec;

        printDebugInfo("Getting choice instance for: "+id);
        if(choiceInstances == null)
            choiceInstances = new HashMap<>();
        printDebugInfo("Length of choiceInstances = "+choiceInstances.size());
        for (Map.Entry<String,Integer> n: choiceInstances.entrySet()){
            printDebugInfo("Choice instance: "+n.getKey()+" "+n.getValue());
        }
        Integer i = choiceInstances.getOrDefault(id, -1);

        if(i.intValue() == -1){
            choiceInstances.put(id, 0);
            return 0;
        }else{
            Integer j = i + 1;
            choiceInstances.put(id, j);
            return j.intValue();
        }
    }

    public static int angelixChooseInt(int bl, int bc, int el, int ec, String[] env_ids, TypeBox[] env_vals,
                                       int evaluated)
    {
        printDebugInfo("Calling angelixChooseInt");
        if(isSymbolicRuntime()){
            printDebugInfo("Running Symbolically");
            int i;
            assert (env_ids.length == env_vals.length);
            int instance = choiceInstanceOf(bl,bc,el,ec);
            // This is to extract environment variables' values
            for (i = 0; i < env_vals.length; i++){
                if(env_vals[i].isIntType()) {
                    int sv;
                    String id = "int"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"env"+VAR_SEPARATOR+env_ids[i];
                    sv = Debug.makeSymbolicInteger(id);
                    Debug.assume(sv == env_vals[i].getIntVal());
                    printDebugInfo(id+" "+env_vals[i].getIntVal());
                }else if(env_vals[i].isDoubleType()){
                    double sv;
                    String id = "double"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"env"+VAR_SEPARATOR+env_ids[i];
                    sv = Debug.makeSymbolicReal(id);
                    Debug.assume(sv == env_vals[i].getDoubleVal());
                }else if(env_vals[i].isFloatType()){
                    double sv;
                    String id = "float"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"env"+VAR_SEPARATOR+env_ids[i];
                    sv = Debug.makeSymbolicReal(id);
                    Debug.assume(sv == (double) env_vals[i].getFloatVal());
                }else if(env_vals[i].isCharType()){
                    int sv;
                    String id = "char"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"env"+VAR_SEPARATOR+env_ids[i];
                    sv = Debug.makeSymbolicInteger(id);
                    Debug.assume(sv == env_vals[i].getCharVal());
                }
            }
            // This is to extract angelic value of the whole expression
            int s;
            String angelicID = "int"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"angelic";
            s = Debug.makeSymbolicInteger(angelicID);
            //String originalID = "int"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"original";
            //int origSymbolic = Debug.makeSymbolicInteger(originalID);
            //Debug.assume(origSymbolic == evaluated);
            //Debug.assume(s != origSymbolic || s == origSymbolic);
            printDebugInfo("angelic: "+angelicID+" "+s);
            return s;
        }else{
            return evaluated;
        }
    }

    public static int angelixChooseChar(int bl, int bc, int el, int ec, String[] env_ids, TypeBox[] env_vals,
                                       char evaluated)
    {
        printDebugInfo("Calling angelixChooseChar");
        if(isSymbolicRuntime()){
            printDebugInfo("Running Symbolically");
            int i;
            assert (env_ids.length == env_vals.length);
            int instance = choiceInstanceOf(bl,bc,el,ec);
            // This is to extract environment variables' values
            for (i = 0; i < env_vals.length; i++){
                if(env_vals[i].isIntType()) {
                    int sv;
                    String id = "int"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"env"+VAR_SEPARATOR+env_ids[i];
                    sv = Debug.makeSymbolicInteger(id);
                    Debug.assume(sv == env_vals[i].getIntVal());
                }else if(env_vals[i].isDoubleType()){
                    double sv;
                    String id = "double"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"env"+VAR_SEPARATOR+env_ids[i];
                    sv = Debug.makeSymbolicReal(id);
                    Debug.assume(sv == env_vals[i].getDoubleVal());
                }else if(env_vals[i].isFloatType()){
                    double sv;
                    String id = "float"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"env"+VAR_SEPARATOR+env_ids[i];
                    sv = Debug.makeSymbolicReal(id);
                    Debug.assume(sv == (double) env_vals[i].getFloatVal());
                }else if(env_vals[i].isCharType()){
                    int sv;
                    String id = "char"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"env"+VAR_SEPARATOR+env_ids[i];
                    sv = Debug.makeSymbolicInteger(id);
                    Debug.assume(sv == env_vals[i].getCharVal());
                }
            }
            // This is to extract angelic value of the whole expression
            int s;
            String angelicID = "char"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"angelic";
            s = Debug.makeSymbolicInteger(angelicID);
            printDebugInfo("Exit angelixChooseChar...");
            return s;
        }else{
            return evaluated;
        }
    }

    public static double angelixChooseDouble(int bl, int bc, int el, int ec, String[] env_ids, TypeBox[] env_vals,
                                       double evaluated)
    {
        printDebugInfo("Calling angelixChooseDouble");
        if(isSymbolicRuntime()){
            printDebugInfo("Running Symbolically");
            int i;
            assert (env_ids.length == env_vals.length);
            int instance = choiceInstanceOf(bl,bc,el,ec);
            // This is to extract environment variables' values
            for (i = 0; i < env_vals.length; i++) {
                if(env_vals[i].isIntType()) {
                    int sv;
                    String id = "int"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"env"+VAR_SEPARATOR+env_ids[i];
                    sv = Debug.makeSymbolicInteger(id);
                    Debug.assume(sv == env_vals[i].getIntVal());
                }else if(env_vals[i].isDoubleType()){
                    double sv;
                    String id = "double"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"env"+VAR_SEPARATOR+env_ids[i];
                    sv = Debug.makeSymbolicReal(id);
                    Debug.assume(sv == env_vals[i].getDoubleVal());
                }else if(env_vals[i].isFloatType()){
                    double sv;
                    String id = "float"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"env"+VAR_SEPARATOR+env_ids[i];
                    sv = Debug.makeSymbolicReal(id);
                    Debug.assume(sv == (double) env_vals[i].getFloatVal());
                }else if(env_vals[i].isCharType()){
                    int sv;
                    String id = "char"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"env"+VAR_SEPARATOR+env_ids[i];
                    sv = Debug.makeSymbolicInteger(id);
                    Debug.assume(sv == env_vals[i].getCharVal());
                }
            }
            // This is to extract angelic value of the whole expression
            double s;
            String angelicID = "double"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"angelic";
            s = Debug.makeSymbolicReal(angelicID);
            return s;
        }else{
            return evaluated;
        }
    }

    public static float angelixChooseFloat(int bl, int bc, int el, int ec, String[] env_ids, TypeBox[] env_vals,
                                             float evaluated)
    {
        printDebugInfo("Calling angelixChooseFloat");
        if(isSymbolicRuntime()){
            printDebugInfo("Running Symbolically");
            int i;
            assert (env_ids.length == env_vals.length);
            int instance = choiceInstanceOf(bl,bc,el,ec);
            // This is to extract environment variables' values
            for (i = 0; i < env_vals.length; i++) {
                if(env_vals[i].isIntType()) {
                    int sv;
                    String id = "int"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"env"+VAR_SEPARATOR+env_ids[i];
                    sv = Debug.makeSymbolicInteger(id);
                    Debug.assume(sv == env_vals[i].getIntVal());
                }else if(env_vals[i].isDoubleType()){
                    double sv;
                    String id = "double"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"env"+VAR_SEPARATOR+env_ids[i];
                    sv = Debug.makeSymbolicReal(id);
                    Debug.assume(sv == env_vals[i].getDoubleVal());
                }else if(env_vals[i].isFloatType()){
                    double sv;
                    String id = "float"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"env"+VAR_SEPARATOR+env_ids[i];
                    printDebugInfo("Symb var: "+id);
                    sv = Debug.makeSymbolicReal(id);
                    Debug.assume(sv == (double) env_vals[i].getFloatVal());
                    printDebugInfo("Get through...");
                }else if(env_vals[i].isCharType()){
                    int sv;
                    String id = "char"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"env"+VAR_SEPARATOR+env_ids[i];
                    sv = Debug.makeSymbolicInteger(id);
                    Debug.assume(sv == env_vals[i].getCharVal());
                }
            }
            // This is to extract angelic value of the whole expression
            double s;
            String angelicID = "float"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"angelic";
            s = Debug.makeSymbolicReal(angelicID);
            return (float) s;
        }else{
            return evaluated;
        }
    }

    public static boolean angelixChooseBool(int bl, int bc, int el, int ec, String[] env_ids, TypeBox[] env_vals,
                                       boolean evaluated){
        printDebugInfo("Calling angelixChooseBool");
        if(isSymbolicRuntime()){
            printDebugInfo("Running Symbolically");
            int i;
            assert (env_ids.length == env_vals.length);
            int instance = choiceInstanceOf(bl,bc,el,ec);
            // This is to extract environment variables' values
            for (i = 0; i < env_vals.length; i++) {
                printDebugInfo("ENVID "+env_ids[i]);
                printDebugInfo("ENVVAL "+env_vals[i].getType());
                if(env_vals[i].isIntType()) {
                    int sv;

                    String id = "int"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"env"+VAR_SEPARATOR+env_ids[i];
                    sv = Debug.makeSymbolicInteger(id);
                    printDebugInfo("Running Symbolically --  var: "+id +" value "+env_vals[i].getIntVal());
                    Debug.assume(sv == env_vals[i].getIntVal());
                }
                else if(env_vals[i].isDoubleType()){
                    double sv;
                    String id = "double"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"env"+VAR_SEPARATOR+env_ids[i];
                    //String id = "kk";//env_ids[i];
                    sv = Debug.makeSymbolicReal(id);
                    printDebugInfo("Running Symbolically --  var: "+id +" value "+env_vals[i].getDoubleVal());
                    Debug.assume(sv == env_vals[i].getDoubleVal());
                }else if(env_vals[i].isBoolType()){
                    boolean sv;
                    String id = "bool"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"env"+VAR_SEPARATOR+env_ids[i];
                    sv = Debug.makeSymbolicBoolean(id);
                    printDebugInfo("Running Symbolically --  var: "+id +" value "+env_vals[i].getBooleanVal());
                    Debug.assume(sv == env_vals[i].getBooleanVal());
                }else if(env_vals[i].isStringType()){
                    /*String sv;
                    String id = "string"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"env"+VAR_SEPARATOR+env_ids[i];
                    printDebugInfo("Running Symbolically --  var: "+id +" value "+env_vals[i].getStringVal());
                    sv = Debug.makeSymbolicString(id);
                    printDebugInfo("Got hereXXX!!!");
                    Debug.assume(sv.equals(env_vals[i].getStringVal()));
                    //Debug.assume(sv.);
                    printDebugInfo("Got here!!!");*/

                    // Feb 9, 2017: we now do string as integer, to handle equality and inequality only
                    String id = "int"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"env"+VAR_SEPARATOR+env_ids[i];
                    printDebugInfo("Running Symbolically --  var: "+id +" value "+env_vals[i].getStringVal());
                    int sv = Debug.makeSymbolicInteger(id);
                    Debug.assume(sv == env_vals[i].getStringVal().hashCode());

                }else if(env_vals[i].isFloatType()){
                    double sv;
                    String id = "float"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"env"+VAR_SEPARATOR+env_ids[i];
                    sv = Debug.makeSymbolicReal(id);
                    Debug.assume(sv == (double) env_vals[i].getFloatVal());
                }else if(env_vals[i].isCharType()){
                    int sv;
                    String id = "char"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"env"+VAR_SEPARATOR+env_ids[i];
                    sv = Debug.makeSymbolicInteger(id);
                    Debug.assume(sv == env_vals[i].getCharVal());
                }
            }
            boolean s;
            // This is to extract angelic value of the whole expression
            String angelicID = "bool"+VAR_SEPARATOR+"choice"+VAR_SEPARATOR+bl+VAR_SEPARATOR+bc+VAR_SEPARATOR+el+VAR_SEPARATOR+ec+VAR_SEPARATOR+instance+VAR_SEPARATOR+"angelic";
            s = Debug.makeSymbolicBoolean(angelicID);
            printDebugInfo("Running Symbolically angelic --  var: "+angelicID);
            //if(DEBUG)
            //    Debug.printPC("DB: PC is ");
            printDebugInfo("*** Exit angelixChooseBool");
            return s;
        }else{
            return evaluated;
        }
    }

    private static String getDumpFile(String id, int instance){
        // These properties are passed through when running test.
        // @See JUnitTestInvoker
        String testNumber = System.getProperty("RUNNING_TEST_NUMBER");
        String angelixFolder = System.getProperty("ANGELIX_FOLDER");
        String dumpFolder = angelixFolder + File.separator + "dump" +File.separator +testNumber+File.separator+id;
        File dumpFolderWithID = new File(dumpFolder);
        if(!dumpFolderWithID.exists())
            dumpFolderWithID.mkdirs();

        String dumpFile = dumpFolder+File.separator+instance;

        return dumpFile;
    }

    public static void angelixReachable(String reachableLabel){
        int instance = 0;
        printDebugInfo("*** CALLING ANGELIX REACHABLE ***");
        if(isDumpingOutput()){
            printDebugInfo("***** Prepare to dump output of reachable******");
            instance = outputInstanceOf("reachable");
            String dumpFile = getDumpFile("reachable", instance);
            try {
                write2File(dumpFile, reachableLabel);
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                return ;
            }
        }else if(isSymbolicRuntime()){
            instance = outputInstanceOf("reachable");
            String dumpFile = getDumpFile("reachable", instance);
            if(new File(dumpFile).exists()) {
                printDebugInfo("Reachable Instance " + instance + " of reachable");
                String s = "reachable"+VAR_SEPARATOR+ reachableLabel +VAR_SEPARATOR+ instance;
                boolean o = Debug.makeSymbolicBoolean(s);
                /**
                 * Put in PC the constraint o. This is the current state.
                 */
                printDebugInfo("reach here");
                Debug.assume(o == true);
                //printDebugInfo("reach here 2");
                try {
                    /**
                     * Put in PC the constraint !o if expected output of reachable does not match with current label.
                     * This is what we expect the program to output.
                     * it causes a backtrack in the state if output does not match with current label
                     */
                    TypeBox expected = expectedOutput(instance, "reachable", TypeBox.Type.STRING_TYPE);
                    printDebugInfo("expected: "+expected.getStringVal());
                    if(expected.getStringVal().compareTo(reachableLabel) != 0) {
                        Debug.printPC("PC with output =" + expected.getStringVal()+" not satisfied ");
                        Debug.assume(o == false);
                    }else{
                        Debug.printPC("PC with output =" + expected.getStringVal()+" satisfied ");
                    }
                    return;
                } catch (IOException e) {
                    e.printStackTrace();
                    throw new RuntimeException("All expected data (output) for both negative and positive tests must have been dumped! But Cannot find dumped (expected) data for test number: " + System.getProperty("RUNNING_TEST_NUMBER"));
                    // if dump is not found, we throw exception
                }
            }else{
                return ;
            }
        }
    }

    // id could be for example stdout
    public static int angelixOutputInt(int output, String id){
        int instance = 0;
        printDebugInfo("***** CALLING ANGELIX OUTPUT INT");
        if(isDumpingOutput()){
            printDebugInfo("***** Prepare to dump output ******");
            instance = outputInstanceOf(id);
            String dumpFile = getDumpFile(id, instance);
            try {
                write2File(dumpFile, Integer.toString(output));
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                return output;
            }
        }
        else if(isSymbolicRuntime()){
            instance = outputInstanceOf(id);
            String dumpFile = getDumpFile(id, instance);
            if(new File(dumpFile).exists()) {
                printDebugInfo("Output Instance " + instance + " of " + id);
                String s = "int"+VAR_SEPARATOR+"output" +VAR_SEPARATOR+ id + VAR_SEPARATOR + instance;
                int o = Debug.makeSymbolicInteger(s);
                /**
                 * Put in PC the constraint o == output. This is the output by current state.
                 */
                Debug.assume(o == output);

                try {
                    /**
                     * Put in PC the constraint o == expected out put. This is what we expect the program to output.
                     * If this makes the PC contradict (with above added PC, which is o == output), it causes
                     * a backtrack in the state, until we find the state in which o == output matched with o == expected.
                     */
                    TypeBox expected = expectedOutput(instance, id, TypeBox.Type.INT_TYPE);
                    Debug.assume(o == expected.getIntVal());
                    Debug.printPC("PC with output1=" + expected.getIntVal());
                } catch (IOException e) {
                    e.printStackTrace();
                    throw new RuntimeException("All expected data (output) for both negative and positive tests must have been dumped! But Cannot find dumped (expected) data for test number: " + System.getProperty("RUNNING_TEST_NUMBER"));
                    // if dump is not found, we throw exception!
                }
                printDebugInfo("Returning symb: "+output+ " symb at this: "+o);
                //Bachle: just now changed return o to return output
                return output;
            }else{
                printDebugInfo("Returning ..: "+output);
                return output;
            }
        }else{
            printDebugInfo("Returning ...: "+output);
            return output;
        }
    }

    public static double angelixOutputDouble(double output, String id){
        int instance = 0;
        printDebugInfo("***** CALLING ANGELIX OUTPUT DOUBLE");
        if(isDumpingOutput()){
            printDebugInfo("***** Prepare to dump output ******");
            instance = outputInstanceOf(id);
            String dumpFile = getDumpFile(id, instance);
            try {
                write2File(dumpFile, Double.toString(output));
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                return output;
            }
        }
        else if(isSymbolicRuntime()){
            instance = outputInstanceOf(id);
            String dumpFile = getDumpFile(id, instance);
            if(new File(dumpFile).exists()) {
                printDebugInfo("Output Instance " + instance + " of " + id);
                String s = "double"+VAR_SEPARATOR+"output" +VAR_SEPARATOR+ id + VAR_SEPARATOR + instance;
                double o = Debug.makeSymbolicReal(s);
                /**
                 * Put in PC the constraint o == output. This is the output by current state.
                 */
                printDebugInfo("****got here1");
                Debug.assume(o == output);
                printDebugInfo("****got here10");
                Debug.printPC("PC is now: ");
                try {
                    /**
                     * Put in PC the constraint o == expected out put. This is what we expect the program to output.
                     * If this makes the PC contradict (with above added PC, which is o == output), it causes
                     * a backtrack in the state, until we find the state in which o == output matched with o == expected.
                     */
                    printDebugInfo("****got here");
                    TypeBox expected = expectedOutput(instance, id, TypeBox.Type.DOUBLE_TYPE);
                    Debug.assume(o == expected.getDoubleVal());
                    printDebugInfo("****got here 2");
                    Debug.printPC("PC with output1=" + expected.getIntVal());
                } catch (IOException e) {
                    e.printStackTrace();
                    throw new RuntimeException("All expected data (output) for both negative and positive tests must have been dumped! But Cannot find dumped (expected) data for test number: " + System.getProperty("RUNNING_TEST_NUMBER"));
                    // if dump is not found, we throw exception!
                }
                // Bachle: just now changed the return from o to output
                return output;
            }else{
                return output;
            }
        }else{
            return output;
        }
    }

    public static char angelixOutputChar(char output, String id){
        int instance = 0;
        printDebugInfo("***** CALLING ANGELIX OUTPUT INT");
        if(isDumpingOutput()){
            printDebugInfo("***** Prepare to dump output ******");
            instance = outputInstanceOf(id);
            String dumpFile = getDumpFile(id, instance);
            try {
                write2File(dumpFile, Integer.toString(output));
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                return output;
            }
        }
        else if(isSymbolicRuntime()){
            instance = outputInstanceOf(id);
            String dumpFile = getDumpFile(id, instance);
            if(new File(dumpFile).exists()) {
                printDebugInfo("Output Instance " + instance + " of " + id);
                String s = "char"+VAR_SEPARATOR+"output" +VAR_SEPARATOR+ id + VAR_SEPARATOR + instance;
                int o = Debug.makeSymbolicInteger(s);
                Debug.assume(o == output);

                try {
                    TypeBox expected = expectedOutput(instance, id, TypeBox.Type.CHAR_TYPE);
                    Debug.assume(o == expected.getCharVal());
                    Debug.printPC("PC with output1=" + expected.getCharVal());
                } catch (IOException e) {
                    e.printStackTrace();
                    throw new RuntimeException("All expected data (output) for both negative and positive tests must have been dumped! But Cannot find dumped (expected) data for test number: " + System.getProperty("RUNNING_TEST_NUMBER"));
                    // if dump is not found, we throw exception!
                }
                printDebugInfo("Returning symb: "+output+ " symb at this: "+o);
                //Bachle: just now changed return o to return output
                return output;
            }else{
                printDebugInfo("Returning ..: "+output);
                return output;
            }
        }else{
            printDebugInfo("Returning ...: "+output);
            return output;
        }
    }

    public static float angelixOutputFloat(float output, String id){
        int instance = 0;
        printDebugInfo("***** CALLING ANGELIX OUTPUT FLOAT");
        if(isDumpingOutput()){
            printDebugInfo("***** Prepare to dump output ******");
            instance = outputInstanceOf(id);
            String dumpFile = getDumpFile(id, instance);
            try {
                write2File(dumpFile, Float.toString(output));
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                return output;
            }
        }
        else if(isSymbolicRuntime()){
            instance = outputInstanceOf(id);
            String dumpFile = getDumpFile(id, instance);
            if(new File(dumpFile).exists()) {
                printDebugInfo("Output Instance " + instance + " of " + id);
                String s = "float"+VAR_SEPARATOR+"output" +VAR_SEPARATOR+ id + VAR_SEPARATOR + instance;
                double o = Debug.makeSymbolicReal(s);
                Debug.assume((o == (double) output));
                printDebugInfo("Through here....");

                try {
                    TypeBox expected = expectedOutput(instance, id, TypeBox.Type.FLOAT_TYPE);
                    printDebugInfo("expected = "+expected.getFloatVal()+" actual = "+output);
                    Debug.assume(o == (double) expected.getFloatVal());
                    Debug.printPC("PC with output1=" + expected.getFloatVal());
                } catch (IOException e) {
                    e.printStackTrace();
                    throw new RuntimeException("All expected data (output) for both negative and positive tests must have been dumped! But Cannot find dumped (expected) data for test number: " + System.getProperty("RUNNING_TEST_NUMBER"));
                    // if dump is not found, we throw exception!
                }
                printDebugInfo("Returning symb: "+output+ " symb at this: "+o);
                //Bachle: just now changed return o to return output
                return output;
            }else{
                printDebugInfo("Returning ..: "+output);
                return output;
            }
        }else{
            printDebugInfo("Returning ...: "+output);
            return output;
        }
    }

    public static boolean angelixOutputBoolean(boolean output, String id){
        int instance = 0;
        printDebugInfo("***** CALLING ANGELIX OUTPUT BOOLEAN");
        if(isDumpingOutput()){
            printDebugInfo("***** Prepare to dump output ******");
            instance = outputInstanceOf(id);
            String dumpFile = getDumpFile(id, instance);
            try {
                write2File(dumpFile, Boolean.toString(output));
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                return output;
            }
        }
        else if(isSymbolicRuntime()){
            instance = outputInstanceOf(id);
            String s = "bool"+VAR_SEPARATOR+"output"+VAR_SEPARATOR+id+VAR_SEPARATOR+instance;
            boolean o = Debug.makeSymbolicBoolean(s);
            Debug.assume(o == output);

            try {
                Debug.assume(o == expectedOutput(instance, id, TypeBox.Type.BOOL_TYPE).getBooleanVal());
            } catch (IOException e) {
                e.printStackTrace();
            }
            // Bachle: just now changed from return o to return output
            return output;
        }else{
            return output;
        }
    }

    public static String angelixOutputString(String output, String id) {
        int instance = 0;
        printDebugInfo("***** CALLING ANGELIX OUTPUT STRING");
        if(isDumpingOutput()){
            printDebugInfo("***** Prepare to dump output ******");
            instance = outputInstanceOf(id);
            String dumpFile = getDumpFile(id, instance);
            try {
                write2File(dumpFile, output);
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                return output;
            }
        }
        else if(isSymbolicRuntime()){
            //printDebugInfo("*** CALLING ANGELIX OUTPUT STRING ***");
            instance = outputInstanceOf(id);
            String s = "string"+VAR_SEPARATOR+"output"+VAR_SEPARATOR+id+VAR_SEPARATOR+instance;
            String o = Debug.makeSymbolicString(s);
            Debug.assume(o.equals(output));

            try {
                Debug.assume(o.equals(expectedOutput(instance, id, TypeBox.Type.STRING_TYPE).getStringVal()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            // Bachle: changed from return o to return output
            return output;
        }else{
            return output;
        }
    }

    public static void write2File(String fileName, String content) throws IOException {
        FileWriter fileWriter = new FileWriter(fileName);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        bufferedWriter.write(content);
        bufferedWriter.close();
    }

    public static String readFile2String(String f) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(f));
        String line = null;
        StringBuffer ans = new StringBuffer();

        while ((line = reader.readLine()) != null) {
            ans.append(line + "\n");
        }

        reader.close();
        return ans.toString();
    }

    // id could be reachable or stdout, or whatever name of output given in angelixOutput and assert file
    private static TypeBox expectedOutput(int instance, String id, TypeBox.Type expectedType) throws IOException {
        String testNumber = System.getProperty("RUNNING_TEST_NUMBER");
        String angelixFolder = System.getProperty("ANGELIX_FOLDER");
        String dumpFolder = angelixFolder + File.separator + "dump";
        String expectedOutputFile = dumpFolder + File.separator + testNumber + File.separator + id + File.separator + instance;
        String expectedOutputRaw = readFile2String(expectedOutputFile);

        // Be careful, we need to trim() for conversion to int, double, or boolean to be correct!
        String expectedOutput = expectedOutputRaw.trim();
        printDebugInfo("Expected Output: " + expectedOutput);
        if (id.compareTo("reachable") == 0)
            return new TypeBox(expectedOutput, TypeBox.Type.STRING_TYPE);

        if (expectedType == TypeBox.Type.INT_TYPE) {
            int i = Integer.parseInt(expectedOutput);
            return new TypeBox(i, TypeBox.Type.INT_TYPE);
        } else if (expectedType == TypeBox.Type.DOUBLE_TYPE) {
            double i = Double.parseDouble(expectedOutput);
            printDebugInfo("Expected Output double: " + i);
            return new TypeBox(i, TypeBox.Type.DOUBLE_TYPE);
        } else if (expectedType == TypeBox.Type.BOOL_TYPE) {
            boolean i = Boolean.parseBoolean(expectedOutput);
            return new TypeBox(i, TypeBox.Type.BOOL_TYPE);
        } else if (expectedType == TypeBox.Type.STRING_TYPE) {
            return new TypeBox(expectedOutput, TypeBox.Type.STRING_TYPE);
        } else if(expectedType == TypeBox.Type.FLOAT_TYPE){
            float f = Float.parseFloat(expectedOutput);
            return new TypeBox(f, TypeBox.Type.FLOAT_TYPE);
        } else{
            throw new RuntimeException("Not yet handled type: "+expectedType);
        }
    }
}
