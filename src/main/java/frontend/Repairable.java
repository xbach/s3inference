package frontend;

import org.eclipse.jdt.core.dom.ITypeBinding;
import driver.Options;

/**
 * Created by dxble on 8/30/16.
 */
public class Repairable {
    public static Boolean isPrimitiveType(ITypeBinding type){
        boolean isPrim = false;
        if(Options.notRepairableTypes().contains("long") && isLongType(type.toString()))
            return false;

        if(Options.repairableTypes().contains("short"))
            isPrim = isShortType(type.toString());
        if(Options.repairableTypes().contains("int"))
            isPrim = isPrim || isIntType(type.toString());
        if(Options.repairableTypes().contains("double"))
            isPrim = isPrim || isDoubleType(type.toString());
        if(Options.repairableTypes().contains("bool"))
            isPrim = isPrim || isBoolType(type.toString());
        //TODO: currently String is treated as non primitive type
        //if(Options.repairableTypes().contains("string"))
        //    isPrim = isPrim || isStringType(type.toString());
        if(Options.repairableTypes().contains("float"))
            isPrim = isPrim || isFloatType(type.toString());
        if(Options.repairableTypes().contains("long"))
            isPrim = isPrim || isLongType(type.toString());

        if(Options.repairableTypes().contains("char"))
            isPrim = isPrim || isCharType(type.toString());
        else if(Options.repairableTypes().contains(type.toString()))
            isPrim = true;
        return isPrim;
    }

    private static boolean isShortType(String type) {
        return type.compareTo("short") == 0;
    }

    public static Boolean canHandleType(ITypeBinding type){
        boolean repairable = false;
        if(!Options.repairEnum() && type.isEnum())
            return false;
        if(type.getName().compareTo("null") == 0)
            return false;

        if(Options.repairableTypes().contains("string"))
            repairable = isStringType(type.toString());

        // Note that handle only checks primitive types. For other, like object, see repairNullness, etc.
        repairable = isPrimitiveType(type) || Options.repairNullness() || repairable;

        return repairable;
    }

    public static Boolean isIntType(String type){
        return type.compareTo("int") == 0 ||
                ((type.compareTo("Integer") == 0 || type.contains("public final class java.lang.Integer")) /*&& Options.repairNullness()*/) ||
                isLongType(type);
    }

    public static Boolean isBoolType(String type){
        return type.compareTo("boolean") == 0;
    }

    public static Boolean isDoubleType(String type){
        return type.compareTo("double") == 0;
    }

    public static Boolean isStringType(String type){
        return type.startsWith("public final class java.lang.String") || type.compareTo("String") == 0;
    }

    public static Boolean isLongType(String type){
        return type.compareTo("long") == 0 || type.startsWith("public final class java.lang.Long");
    }

    public static Boolean isFloatType(String type){
        return type.compareTo("float") == 0;
    }

    public static Boolean isCharType(String type){
        return type.compareTo("char") == 0;
    }

    public static String type2TypeBoxEnum(ITypeBinding type){
        // This is a workaround for null check
        if(type == null)
            return "BOOL_TYPE";

        // STRING_TYPE, INT_TYPE, BOOL_TYPE, DOUBLE_TYPE
        if(/*type.getName().compareTo("int") == 0*/ isIntType(type.toString()) || isShortType(type.toString()))
            return "INT_TYPE";
        else if(type.getName().compareTo("double") == 0)
            return "DOUBLE_TYPE";
        else if(type.getName().compareTo("boolean") == 0){
            return "BOOL_TYPE";
        }else if(type.getName().compareTo("String") == 0)
            return "STRING_TYPE";
        else if(type.getName().compareTo("float") == 0)
            return "FLOAT_TYPE";
        else if(type.getName().compareTo("char") == 0)
            return "CHAR_TYPE";
        else
            throw new RuntimeException("Not yet handle type: "+type);
    }
}
