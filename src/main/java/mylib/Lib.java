package mylib;


import java.io.*;
import java.lang.reflect.Method;
import java.nio.channels.FileChannel;
import java.util.*;

public class Lib {
    public Lib() {
    }

    public static boolean initializeFolder(String f) {
        File d = new File(f);
        if (d.isDirectory()) {
            delete(d);
        }

        return d.mkdirs();
    }

    public static boolean folderExist(String f) {
        File d = new File(f);
        return d.exists() && d.isDirectory();
    }

    public static String removeLineNumber(String label) {
        if (label == null) {
            return null;
        } else {
            StringTokenizer stn = new StringTokenizer(label, "_");

            ArrayList tokens;
            String tk;
            for(tokens = new ArrayList(); stn.hasMoreTokens(); tokens.add(tk)) {
                tk = stn.nextToken();
                if (tk.startsWith("EXIT") && Character.isDigit(tk.charAt(tk.length() - 1))) {
                    tk = "EXIT";
                }
            }

            StringBuffer buff = new StringBuffer();
            buff.append((String)tokens.get(0));

            for(int i = 1; i < tokens.size(); ++i) {
                buff.append("_" + (String)tokens.get(i));
            }

            return buff.toString().trim();
        }
    }

    public static String lowerCaseFirstChar(String clazzName) {
        StringBuffer buff = new StringBuffer(clazzName);
        buff.setCharAt(0, Character.toLowerCase(clazzName.charAt(0)));
        return buff.toString();
    }

    public static void delete(File d) {
        if (d.isDirectory()) {
            File[] arr$ = d.listFiles();
            int len$ = arr$.length;

            for(int i$ = 0; i$ < len$; ++i$) {
                File f = arr$[i$];
                delete(f);
            }
        }

        d.delete();
    }


    public static List<File> getFilesEecursively(File f) {
        List<File> ans = new LinkedList();
        if (f.isDirectory()) {
            File[] arr$ = f.listFiles();
            int len$ = arr$.length;

            for(int i$ = 0; i$ < len$; ++i$) {
                File c = arr$[i$];
                ans.addAll(getFilesEecursively(c));
            }
        } else if (f.isFile()) {
            ans.add(f);
        }

        return ans;
    }

    public static String getFileExtension(File f) {
        int dot = f.getName().lastIndexOf(46);
        return dot >= 0 ? f.getName().substring(dot + 1) : null;
    }

    public static String readFile2String(String f) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(f));
        String line = null;
        StringBuffer ans = new StringBuffer();

        while((line = reader.readLine()) != null) {
            ans.append(line + "\n");
        }

        reader.close();
        return ans.toString();
    }

    public static void writeText2File(String st, File f) throws IOException {
        PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(f)));
        writer.println(st);
        writer.close();
    }

    public static String[] readFile2Lines(File f) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(f));
        List<String> lines = new LinkedList();
        String line = null;

        while((line = reader.readLine()) != null) {
            lines.add(line);
        }

        reader.close();
        return (String[])lines.toArray(new String[0]);
    }

    public static void writeList2File(List<String> lines, String outputFixedBugs) throws IOException {
        PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(outputFixedBugs)));
        Iterator i$ = lines.iterator();

        while(i$.hasNext()) {
            String st = (String)i$.next();
            writer.println(st);
        }

        writer.close();
    }

    public static List<File> searchDFS4Files(File f, String extension) {
        List<File> ans = new LinkedList();
        if (f.isFile()) {
            String ext = getExtension(f);
            if (ext != null && ext.compareTo(extension) == 0) {
                ans.add(f);
            }
        } else if (f.isDirectory()) {
            File[] arr$ = f.listFiles();
            int len$ = arr$.length;

            for(int i$ = 0; i$ < len$; ++i$) {
                File c = arr$[i$];
                ans.addAll(search4Files(c, extension));
            }
        } else {
            System.out.println("ERROR: " + f + " does not exist");
            System.exit(0);
        }

        return ans;
    }

    public static List<File> search4Folder(File dir, String dName) {
        List<File> ans = new LinkedList();
        Queue<File> queue = new LinkedList();
        queue.add(dir);

        while(true) {
            File e;
            do {
                if (queue.isEmpty()) {
                    return ans;
                }

                e = (File)queue.poll();
            } while(!e.isDirectory());

            if (e.getName().compareTo(dName) == 0) {
                ans.add(e);
            }

            File[] lst = e.listFiles();
            File[] arr$ = lst;
            int len$ = lst.length;

            for(int i$ = 0; i$ < len$; ++i$) {
                File f = arr$[i$];
                queue.add(f);
            }
        }
    }

    public static List<File> search4Files(File dir, String extension) {
        List<File> ans = new LinkedList();
        Queue<File> queue = new LinkedList();
        queue.add(dir);

        while(true) {
            while(!queue.isEmpty()) {
                File e = (File)queue.poll();
                if (e.isFile()) {
                    if (e.getName().endsWith("." + extension)) {
                        ans.add(e);
                    }
                } else {
                    File[] lst = e.listFiles();
                    File[] arr$ = lst;
                    int len$ = lst.length;

                    for(int i$ = 0; i$ < len$; ++i$) {
                        File f = arr$[i$];
                        queue.add(f);
                    }
                }
            }

            return ans;
        }
    }

    public static List<File> search4FilesByName(File dir, String fullname) {
        List<File> ans = new LinkedList();
        Queue<File> queue = new LinkedList();
        queue.add(dir);

        while(true) {
            while(!queue.isEmpty()) {
                File e = (File)queue.poll();
                if (e.isFile()) {
                    if (e.getName().compareTo(fullname) == 0) {
                        ans.add(e);
                    }
                } else {
                    File[] lst = e.listFiles();
                    File[] arr$ = lst;
                    int len$ = lst.length;

                    for(int i$ = 0; i$ < len$; ++i$) {
                        File f = arr$[i$];
                        queue.add(f);
                    }
                }
            }

            return ans;
        }
    }

    public static List<File> search4FilesContainName(File dir, String containName) {
        List<File> ans = new LinkedList();
        Queue<File> queue = new LinkedList();
        queue.add(dir);

        while(true) {
            while(!queue.isEmpty()) {
                File e = (File)queue.poll();
                if (e.isFile()) {
                    if (e.getName().contains(containName) && !e.getName().endsWith("~")) {
                        ans.add(e);
                    }
                } else {
                    File[] lst = e.listFiles();
                    File[] arr$ = lst;
                    int len$ = lst.length;

                    for(int i$ = 0; i$ < len$; ++i$) {
                        File f = arr$[i$];
                        queue.add(f);
                    }
                }
            }

            return ans;
        }
    }

    private static String getExtension(File f) {
        int dot = f.getName().lastIndexOf(46);
        return dot >= 0 ? f.getName().substring(dot + 1) : null;
    }

    public static void mergeFiles(List<File> interestedTraces, File finalTrace) throws IOException {
        PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(finalTrace)));
        Iterator i$ = interestedTraces.iterator();

        while(i$.hasNext()) {
            File f = (File)i$.next();
            System.out.println("Reading ... " + f);
            String str = readFile2String(f.getPath());
            System.out.println("Writng ... " + f);
            writer.println(str);
        }

        writer.close();
    }

    public static String getBaseName(String f) {
        int dot = f.lastIndexOf(46);
        return dot < 0 ? null : f.substring(0, dot);
    }

    public static List<String> readFile2List(String f) throws IOException {
        List<String> lst = new LinkedList();
        BufferedReader reader = new BufferedReader(new FileReader(f));
        String line = null;

        while((line = reader.readLine()) != null) {
            lst.add(line);
        }

        reader.close();
        return new ArrayList(lst);
    }

    public static String cutFileName(File container, File f) throws IOException {
        String path = f.getCanonicalPath();
        String outsidePath = container.getCanonicalPath();

        String cutPath;
        for(cutPath = path.replace(outsidePath, ""); cutPath.length() > 0 && cutPath.charAt(0) == File.separatorChar; cutPath = cutPath.substring(1)) {
            ;
        }

        return cutPath;
    }

    public static void copyFile(File a, File b) throws IOException {
        FileInputStream inStream;
        FileChannel in = (inStream = new FileInputStream(a)).getChannel();
        FileOutputStream outStream;
        FileChannel out = (outStream = new FileOutputStream(b)).getChannel();
        out.transferFrom(in, 0L, in.size());
        in.close();
        out.close();
        inStream.close();
        outStream.close();
    }


    public static Method getMethod(Class<?> target, String name) {
        Method[] mts = target.getDeclaredMethods();
        Method[] arr$ = mts;
        int len$ = mts.length;

        for(int i$ = 0; i$ < len$; ++i$) {
            Method m = arr$[i$];
            String st = m.getName();
            if (m.getName().compareTo(name) == 0) {
                return m;
            }
        }

        return null;
    }

    public static String getExtension(String name) {
        Integer index = name.lastIndexOf(46);
        return index < 0 ? null : name.substring(index + 1);
    }

    public static void copyFolder(File src, File dest) throws IOException {
        List<File> files = listAllFiles(src);
        Iterator i$ = files.iterator();

        while(i$.hasNext()) {
            File f = (File)i$.next();
            System.out.println("Copying " + f);
            String cutname = cutFileName(src, f);
            File outputFile = new File(dest, cutname);
            outputFile.getParentFile().mkdirs();
            copyFile(f, outputFile);
        }

    }

    private static List<File> listAllFiles(File d) {
        List<File> ans = new LinkedList();
        if (d.isFile()) {
            ans.add(d);
        } else if (d.isDirectory()) {
            File[] arrs = d.listFiles();
            int len = arrs.length;

            for(int i = 0; i < len; ++i) {
                File c = arrs[i];
                ans.addAll(listAllFiles(c));
            }
        }

        return ans;
    }

    public static ArrayList<File> walk(String path, String to_find, ArrayList<File> found) {
        File root = new File(path);
        File[] list = root.listFiles();
        if (list == null) {
            return null;
        } else {
            File[] arr$ = list;
            int len$ = list.length;

            for(int i$ = 0; i$ < len$; ++i$) {
                File f = arr$[i$];
                if (f.isDirectory()) {
                    walk(f.getAbsolutePath(), to_find, found);
                } else {
                    String fname = f.getAbsoluteFile().toString();
                    if (fname.contains(to_find)) {
                        found.add(f);
                    }
                }
            }

            return found;
        }
    }

    public static boolean removeDirectory(File directory) {
        if (directory == null) {
            return false;
        } else if (!directory.exists()) {
            return true;
        } else if (!directory.isDirectory()) {
            return false;
        } else {
            String[] list = directory.list();
            if (list != null) {
                for(int i = 0; i < list.length; ++i) {
                    File entry = new File(directory, list[i]);
                    if (entry.isDirectory()) {
                        if (!removeDirectory(entry)) {
                            return false;
                        }
                    } else if (!entry.delete()) {
                        return false;
                    }
                }
            }

            return directory.delete();
        }
    }

    public static File getCorrespondingSourceFile(String filename, Map<String, List<File>> index, File container) throws IOException {
        String basename = filename.substring(filename.lastIndexOf(47) + 1);
        List<File> lst = (List)index.get(basename);
        if (lst == null) {
            return null;
        } else {
            Iterator i$ = lst.iterator();

            File f;
            String path;
            do {
                if (!i$.hasNext()) {
                    return null;
                }

                f = (File)i$.next();
                cutFileName(container, f);
                path = f.getCanonicalPath();
                path = path.replace(File.separatorChar, '/');
            } while(path.indexOf(filename) < 0);

            return f;
        }
    }

    public static int countFiles(File dir, String extension) {
        Queue<File> queue = new LinkedList();
        queue.add(dir);
        int ans = 0;

        while(true) {
            while(!queue.isEmpty()) {
                File e = (File)queue.poll();
                if (e.isFile()) {
                    if (e.getName().endsWith("." + extension)) {
                        ++ans;
                    }
                } else {
                    File[] lst = e.listFiles();
                    File[] arr$ = lst;
                    int len$ = lst.length;

                    for(int i$ = 0; i$ < len$; ++i$) {
                        File f = arr$[i$];
                        queue.add(f);
                    }
                }
            }

            return ans;
        }
    }

    public static String removeBooleanSigns(String label) {
        if (label == null) {
            return label;
        } else {
            StringTokenizer stn = new StringTokenizer(label, "_");
            ArrayList tokens = new ArrayList();

            while(stn.hasMoreTokens()) {
                tokens.add(stn.nextToken());
            }

            StringBuffer buff = new StringBuffer();
            buff.append((String)tokens.get(0));

            for(int i = 1; i < tokens.size(); ++i) {
                if (((String)tokens.get(i)).compareTo("TRUE") != 0 && ((String)tokens.get(i)).compareTo("FALSE") != 0) {
                    buff.append("_" + (String)tokens.get(i));
                }
            }

            return buff.toString();
        }
    }
}
