package tracewriter;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by dxble on 8/28/16.
 */
public class TraceWriter {
    public static void trace_writer(int bl, int bc, int el, int ec){
        try
        {
            String filename= System.getProperty("ANGELIX_TRACE");
            FileWriter fw = new FileWriter(filename,true); //the true will append the new data
            fw.write(bl+" "+bc+" "+el+" "+ec+"\n");//appends the string to the file
            fw.close();
        }
        catch(IOException ioe)
        {
            //System.err.println("IOException: " + ioe.getMessage());
        }
    }

    public static boolean trace_boolean(int bl, int bc, int el, int ec, boolean evaluated){
        trace_writer(bl, bc, el, ec);
        return evaluated;
    }

    public static int trace_int(int bl, int bc, int el, int ec, int evaluated){
        trace_writer(bl, bc, el, ec);
        return evaluated;
    }

    public static double trace_double(int bl, int bc, int el, int ec, double evaluated){
        trace_writer(bl, bc, el, ec);
        return evaluated;
    }

    public static String trace_string(int bl, int bc, int el, int ec, String evaluated){
        trace_writer(bl, bc, el, ec);
        return evaluated;
    }

    public static float trace_float(int bl, int bc, int el, int ec, float evaluated){
        trace_writer(bl, bc, el, ec);
        return evaluated;
    }

    public static char trace_char(int bl, int bc, int el, int ec, char evaluated){
        trace_writer(bl, bc, el, ec);
        return evaluated;
    }
}
