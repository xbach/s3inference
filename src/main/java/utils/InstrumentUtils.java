package utils;

import frontend.Repairable;
import org.eclipse.jdt.core.dom.*;
import localization.FaultIdentifier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by dxble on 8/28/16.
 */
public class InstrumentUtils {
    public static HashMap<String, MethodInvocation> createdMethodCalls = new HashMap<>();
    public static HashMap<String, InfixExpression> createdNullCheck = new HashMap<>();

    public static MethodInvocation createPrint(CompilationUnit cu, String text) {
        MethodInvocation mi =  cu.getAST().newMethodInvocation();
        mi.setExpression(mi.getAST().newName("System.out"));
        mi.setName(mi.getAST().newSimpleName("println"));
        StringLiteral st = mi.getAST().newStringLiteral();
        st.setLiteralValue(text);
        mi.arguments().add(st);
        return mi;
    }

    public static ImportDeclaration createImportTraceWriter(CompilationUnit cu){
        ImportDeclaration ip = cu.getAST().newImportDeclaration();
        ip.setName(cu.getAST().newName(new String[] {"tracewriter","TraceWriter"}));
        return ip;
    }

    public static ImportDeclaration createImportRunTime(CompilationUnit cu){
        ImportDeclaration ip = cu.getAST().newImportDeclaration();
        ip.setName(cu.getAST().newName(new String[] {"backend","RunTime"}));
        return ip;
    }

    public static ImportDeclaration createImport(CompilationUnit cu, String qualifiedName){
        ImportDeclaration ip = cu.getAST().newImportDeclaration();
        ip.setName(cu.getAST().newName(qualifiedName));
        return ip;
    }

    public static ImportDeclaration createImportTypeBox(CompilationUnit cu){
        ImportDeclaration ip = cu.getAST().newImportDeclaration();
        ip.setName(cu.getAST().newName(new String[] {"backend","TypeBox"}));
        return ip;
    }

    public static VariableDeclarationExpression createTesterInstance(CompilationUnit cu, String className, String classFullName, Boolean hasConstructor, int numArgs) {
        VariableDeclarationFragment fragment = cu.getAST().newVariableDeclarationFragment();
        fragment.setName(cu.getAST().newSimpleName("tester"));// cu.getAST().newSimpleName(className)
        ClassInstanceCreation i = createNewClassCreation(cu, className);
        // Current hack to create test that has one constructor with string argument (test name as argument)
        if(hasConstructor && numArgs == 1) {
            StringLiteral arg = i.getAST().newStringLiteral();
            arg.setLiteralValue(classFullName);
            i.arguments().add(arg);
        }

        fragment.setInitializer(i);
        VariableDeclarationExpression cvd = cu.getAST().newVariableDeclarationExpression(fragment);

        cvd.setType(cvd.getAST().newSimpleType(cvd.getAST().newSimpleName(className)));
        return cvd;
    }

    public static ClassInstanceCreation createNewClassCreation(CompilationUnit cu, String className){
        ClassInstanceCreation i = cu.getAST().newClassInstanceCreation();
        i.setType(i.getAST().newSimpleType(i.getAST().newSimpleName(className)));
        return i;
    }

    public static MethodInvocation createMethodCallNoArgs(CompilationUnit cu, String methodName, String exp){
        MethodInvocation methodCall = cu.getAST().newMethodInvocation();
        methodCall.setExpression(cu.getAST().newSimpleName(exp));
        methodCall.setName(cu.getAST().newSimpleName(methodName));
        return methodCall;
    }

    public static MethodInvocation createTraceNode(ASTNode node, String traceMethodName, Boolean addNode2Trace) {
        MethodInvocation methodCall = node.getAST().newMethodInvocation();
        methodCall.setExpression(node.getAST().newSimpleName("TraceWriter"));
        methodCall.setName(node.getAST().newSimpleName(traceMethodName));

        FaultIdentifier id = ASTUtils.getFaultIdentifier(node);

        NumberLiteral blNode = node.getAST().newNumberLiteral();
        blNode.setToken(id.getBeginLine());
        methodCall.arguments().add(blNode);
        NumberLiteral bcNode = node.getAST().newNumberLiteral();
        bcNode.setToken(id.getBeginColumn());
        methodCall.arguments().add(bcNode);
        NumberLiteral elNode = node.getAST().newNumberLiteral();
        elNode.setToken(id.getEndLine());
        methodCall.arguments().add(elNode);
        NumberLiteral ecNode = node.getAST().newNumberLiteral();
        ecNode.setToken(id.getEndColumn());
        methodCall.arguments().add(ecNode);
        if(addNode2Trace)
            methodCall.arguments().add(ASTNode.copySubtree(methodCall.getAST(), node));

        return methodCall;
    }

    public static IfStatement createGuardNode(MethodInvocation choose, Statement stmt){
        IfStatement ifStmt = choose.getAST().newIfStatement();
        ifStmt.setExpression(choose);
        ifStmt.setThenStatement((Statement) ASTNode.copySubtree(ifStmt.getAST(),stmt));
        return ifStmt;
    }

    public static Block addStatement2Block(Block bl, ASTNode node){
        bl.statements().add(node);
        return bl;
    }

    public static MethodInvocation addArgument2MethodCall(MethodInvocation mi, ASTNode node){
        mi.arguments().add(node);
        return mi;
    }

    public static MethodInvocation createChooseNode(String chooseMethodName, FaultIdentifier id, LinkedHashMap<String, ITypeBinding> envVals, ArrayList<String> envIDs) {
        // We create an AST node of angelixChoose. See RunTime.angelixChoose##type
        ASTNode node = id.getJavaNode();
        // To create Runtime.angelixChoose##type
        MethodInvocation methodCall = node.getAST().newMethodInvocation();
        methodCall.setExpression(node.getAST().newSimpleName("RunTime"));
        methodCall.setName(node.getAST().newSimpleName(chooseMethodName));

        // First 4 arguments of angelixChoose
        NumberLiteral blNode = node.getAST().newNumberLiteral();
        blNode.setToken(id.getBeginLine());
        methodCall.arguments().add(blNode);
        NumberLiteral bcNode = node.getAST().newNumberLiteral();
        bcNode.setToken(id.getBeginColumn());
        methodCall.arguments().add(bcNode);
        NumberLiteral elNode = node.getAST().newNumberLiteral();
        elNode.setToken(id.getEndLine());
        methodCall.arguments().add(elNode);
        NumberLiteral ecNode = node.getAST().newNumberLiteral();
        ecNode.setToken(id.getEndColumn());
        methodCall.arguments().add(ecNode);

        // String array of environment id, which contains visible variables' names
        ArrayCreation envIDArray = node.getAST().newArrayCreation();
        SimpleType type = envIDArray.getAST().newSimpleType(envIDArray.getAST().newSimpleName("String"));
        ArrayType atype = envIDArray.getAST().newArrayType(type);
        envIDArray.setType(atype);
        ArrayInitializer ai = envIDArray.getAST().newArrayInitializer();
        for(String envID: envIDs){
            StringLiteral st = envIDArray.getAST().newStringLiteral();
            st.setLiteralValue(envID);
            ai.expressions().add(st);
        }
        envIDArray.setInitializer(ai);

        methodCall.arguments().add(envIDArray);

        ArrayCreation envValArray = node.getAST().newArrayCreation();
        SimpleType typeBox = envValArray.getAST().newSimpleType(envValArray.getAST().newSimpleName("TypeBox"));
        ArrayType atypeBox = envValArray.getAST().newArrayType(typeBox);
        envValArray.setType(atypeBox);
        ArrayInitializer aiEnvVal = envIDArray.getAST().newArrayInitializer();

        // Create TypeBox[] {new TypeBox(.,.), new TypeBox(...),...}
        for(Map.Entry<String, ITypeBinding> entry: envVals.entrySet()){
            ClassInstanceCreation ci = node.getAST().newClassInstanceCreation();
            ci.setType((SimpleType)ASTNode.copySubtree(ci.getAST(),typeBox));
            if(entry.getKey().contains("(") && entry.getKey().contains(")")){
                // Handle method call here
                ci.arguments().add(createdMethodCalls.get(entry.getKey()));
            }
            else if(entry.getKey().contains("!=")){// Handle null check expression
                ci.arguments().add(createdNullCheck.get(entry.getKey()));
            }
            // This is to handle access of field, e.g., this.myField
            else if(entry.getKey().contains("this.")){
                FieldAccess fa = ci.getAST().newFieldAccess();
                fa.setExpression(ci.getAST().newThisExpression());
                fa.setName(ci.getAST().newSimpleName(entry.getKey().replace("this.","")));
                ci.arguments().add(fa);
            }else  {
                ci.arguments().add(ci.getAST().newName(entry.getKey()));
            }

            FieldAccess fa = ci.getAST().newFieldAccess();
            Name qn = fa.getAST().newName(new String[] {"TypeBox","Type"});
            fa.setExpression(qn);
            String typeBoxEnum = Repairable.type2TypeBoxEnum(entry.getValue());
            fa.setName(fa.getAST().newSimpleName(typeBoxEnum));
            ci.arguments().add(fa);

            aiEnvVal.expressions().add(ci);
        }
        envValArray.setInitializer(aiEnvVal);

        methodCall.arguments().add(envValArray);

        if(node instanceof Statement){
            // Statement level. We add the guard true to each statement
            methodCall.arguments().add(methodCall.getAST().newBooleanLiteral(true));
        }else{
            // Expression level
            methodCall.arguments().add(ASTNode.copySubtree(methodCall.getAST(), node));
        }

        return methodCall;
    }

    public static Expression createNameFromString(String name, CompilationUnit ci) {
        if(name.contains("this.")){
            FieldAccess fa = ci.getAST().newFieldAccess();
            fa.setExpression(ci.getAST().newThisExpression());
            fa.setName(ci.getAST().newSimpleName(name.replace("this.","")));
            return fa;
        }else{
            return ci.getAST().newName(name);
        }
    }
}
