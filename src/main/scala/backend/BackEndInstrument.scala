package backend

import java.io.File
import java.net.URL

import backend.runtime.{MethodCallRunTime, ObjectRunTime}
import driver.{Options, S3Properties}
import junithandler.compiler.JavaJDKCompiler
import localization.FaultIdentifier
import utils._
import org.eclipse.jdt.core.dom._
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite
import parser.javaparser.JavaParser

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import testrunner.TestCase
import org.apache.log4j.Logger
/**
  * Created by dxble on 8/30/16.
  */

class BackEndInstrument(faults: ArrayBuffer[FaultIdentifier]){
  val logger = Logger.getLogger(this.getClass)
  val modifiedRewriter:  mutable.LinkedHashMap[String, ASTRewrite] = new mutable.LinkedHashMap[String, ASTRewrite]()
  val modifiedRewriterContent:  mutable.LinkedHashMap[String, String] = new mutable.LinkedHashMap[String, String]()

  private def getRewriterUptoDate(fileName: String): ASTRewrite = {
    if(!modifiedRewriter.contains(fileName)){
      val cu = JavaParser.getCompilationUnit(fileName)
      val original: ASTRewrite = ASTRewrite.create(cu.getAST())

      // Insert the import backend RunTime and TypeBox to the rewriter
      val ipRT = InstrumentUtils.createImportRunTime(cu)
      val ipTB = InstrumentUtils.createImportTypeBox(cu)
      val lrw = original.getListRewrite(cu, CompilationUnit.IMPORTS_PROPERTY)
      lrw.insertLast(ipRT, null)
      lrw.insertLast(ipTB, null)

      modifiedRewriter.put(fileName, original)
    }
    return modifiedRewriter.getOrElse(fileName, null)
  }

  // Replace node by replacement
  private def replaceNode(rew: ASTRewrite, node: ASTNode, replacement: ASTNode): ASTRewrite = {
    var rep: ASTNode = null
    rep = ASTNode.copySubtree(node.getAST(), replacement)
    rew.replace(node, rep, null)
    return rew
  }

  def instrumentChosenLocations(): Unit = {
    val envVals: java.util.LinkedHashMap[String, ITypeBinding] = new java.util.LinkedHashMap[String, ITypeBinding]
    val envIDs: java.util.ArrayList[String] = new java.util.ArrayList[String]
    faults.foreach(f => instrumentOneLocation(f, envIDs, envVals))
  }

  private def findSurroundingClass(node: ASTNode): ASTNode = {
    var surroundingClass = node.getParent
    while (!surroundingClass.isInstanceOf[TypeDeclaration]){
      surroundingClass = surroundingClass.getParent
    }
    return surroundingClass
  }

  // TODO: We are assuming that we are fixing faults at the same (one) file
  private def instrumentOneLocation(fault: FaultIdentifier, envIDs: java.util.ArrayList[String], envVals: java.util.LinkedHashMap[String, ITypeBinding]): Unit = {
    val ast: CompilationUnit = JavaParser.getCompilationUnit(fault.getFileName)
    val faultNode: ASTNode = if(fault.getJavaNode() == null) {
      val fn = ASTUtils.findNode(ast, fault)
      fault.setJavaNode(fn)
      fn
    }else{
      fault.getJavaNode()
    }

    logger.info("Instrumenting faulty node: " + fault)

    val visibleV = new VisibleVars(fault)
    //ast.accept(visibleV)
    val surroundingClass=findSurroundingClass(faultNode)
    surroundingClass.accept(visibleV)
    fault.setLocalVisibleVars(visibleV.otherVisibleVars)
    fault.setStaticFieldVars(visibleV.staticFieldNames)
    fault.setNonStaticFieldVars(visibleV.nonStaticFieldNames)

    // This is to collect visible vars at current fault location.
    // It sets visible vars to the fault, which is given to the visitor as parameter
    //val visibleVarsCollector: VisibleVarsCollector = new VisibleVarsCollector(fault, true)
    //ast.accept(visibleVarsCollector)

    // This can only be called after we collected visible vars already
    // Note that vars use through out the file can be vars from other file, whereas visible vars
    // collected by visibleVarsCollector are only the vars visible in current file at fault
    val usedVarsWholeFile = new UsedVariables()
    ast.accept(usedVarsWholeFile)
    val varsUsedFileAvailAtFault: scala.collection.mutable.Map[JDTNameWrapper, ITypeBinding] = usedVarsWholeFile.availableVarsAtFault(fault)

    val usedVarsAtFault = new UsedVariables()
    fault.getJavaNode().accept(usedVarsAtFault)
    fault.setUsedVarsAtFault(usedVarsAtFault.varsUsed)

    val objRt = new ObjectRunTime(ast, fault)
    def repairNullness(name: String) = {
      // Encode null check as a boolean variable, add to envIDs and envVals
      if (Options.repairNullness) {
        objRt.nullCheck(envIDs, envVals, name)
      }
    }

    import scala.collection.JavaConversions._

    if(!fault.surroundingMethodIsStatic() && Options.collectFields) {
      // Non static field can be accessed by this expression
      for (entry <- fault.getNonStaticFieldVars().entrySet) {
        //import scala.collection.JavaConversions._
        for (t <- entry.getValue) {
          if (frontend.Repairable.canHandleType(t)) {
            if (frontend.Repairable.isPrimitiveType(t) || frontend.Repairable.isStringType(t.toString)) {
              // For now name mangling does nothing, just returns entry.getKey().
              // But we need to consider mangling name later
              val mangledVarName: String = NameMangling.mangleVariable(entry.getKey.getFullyQualifiedName, "FIELDVAR")
              if (!envIDs.contains("this." + mangledVarName) && !Options.notInstrumentVars.contains(mangledVarName)) {
                // This is to differentiate between method variable and field variable that have the same name
                envIDs.add("this." + mangledVarName)
                envVals.put("this." + entry.getKey.getFullyQualifiedName, t)
              }
            } else {
              // Object type
              // For now, we only consider null check
              val name = entry.getKey.getFullyQualifiedName()
              repairNullness(name)
            }
          }
        }
      }
    }
    if(Options.collectFields) {
      // Static field can only be accessed as ClassName.FIELD
      for (entry <- fault.getStaticFieldVars().entrySet) {
        //import scala.collection.JavaConversions._
        for (t <- entry.getValue) {
          if (frontend.Repairable.canHandleType(t)) {
            if (frontend.Repairable.isPrimitiveType(t) || frontend.Repairable.isStringType(t.toString)) {
              // For now name mangling does nothing, just returns entry.getKey().
              // But we need to consider mangling name later
              val mangledVarName: String = NameMangling.mangleVariable(entry.getKey.getFullyQualifiedName, "FIELDVAR")
              // Make sure we do not add duplicate variables
              if (!envIDs.contains(fault.getFileName() + "." + mangledVarName) && !Options.notInstrumentVars.contains(mangledVarName)) {
                // This is to differentiate between method variable and field variable that have the same name
                envIDs.add(fault.getFileName() + "." + mangledVarName)
                envVals.put(fault.getFileName() + "." + entry.getKey.getFullyQualifiedName, t)
              }
            } else {
              // Object type
              // For now, we only consider null check
              val name = entry.getKey.getFullyQualifiedName()
              repairNullness(name)
            }
          }
        }
      }
    }

    import scala.collection.JavaConversions._

    if(Options.collectLocalVisibleVars) {
      // this contains local vars that are visible. By default, we always collect these vars
      for (entry <- fault.getLocalVisibleVars.entrySet) {
        import scala.collection.JavaConversions._
        for (t <- entry.getValue) {
          if (frontend.Repairable.canHandleType(t) || frontend.Repairable.isStringType(t.toString)) {
            if (frontend.Repairable.isPrimitiveType(t)) {
              val mangledVarName: String = NameMangling.mangleVariable(entry.getKey.getFullyQualifiedName, "METHODVAR")
              // Make sure we do not add duplicate variables
              if (!envIDs.contains(mangledVarName) && !Options.notInstrumentVars.contains(mangledVarName)) {
                envIDs.add(mangledVarName)
                envVals.put(entry.getKey.getFullyQualifiedName, t)
              }
            } else {
              // Object type
              // For now, we only consider null check
              val name = entry.getKey.getFullyQualifiedName()
              repairNullness(name)
            }
          }
        }
      }
    }

    if(Options.collectUsedVarAtFault) {
      for (entry <- fault.getUsedVarsAtFault().entrySet) {
        val t = entry.getValue
        if (frontend.Repairable.canHandleType(t) /*&& varNotFromCurrentMethod(fault, entry.getKey)*/) {
          if (frontend.Repairable.isPrimitiveType(t) || frontend.Repairable.isStringType(t.toString)) {
            val mangledVarName: String = NameMangling.mangleVariable(entry.getKey.getFullyQualifiedName, "USEDVAR")
            // Make sure we do not add duplicate variables
            if (!envIDs.contains(mangledVarName) && !Options.notInstrumentVars.contains(mangledVarName)) {
              envIDs.add(mangledVarName)
              envVals.put(entry.getKey.getFullyQualifiedName, t)
            }
          } else {
            // Object type
            // For now, we only consider null check
            val name = entry.getKey.getFullyQualifiedName()
            repairNullness(name)
          }
        }
      }
    }

    if(Options.collectUsedVarsWholeFile) {
      for (entry <- varsUsedFileAvailAtFault) {
        val t = entry._2
        if (frontend.Repairable.canHandleType(t) && varNotFromCurrentMethod(fault, entry._1)) {
          if (frontend.Repairable.isPrimitiveType(t) || frontend.Repairable.isStringType(t.toString)) {
            val mangledVarName: String = NameMangling.mangleVariable(entry._1.getFullyQualifiedName, "USEDVARAVAIL")
            // Make sure we do not add duplicate variables
            if (!envIDs.contains(mangledVarName) && !Options.notInstrumentVars.contains(mangledVarName)) {
              envIDs.add(mangledVarName)
              envVals.put(entry._1.getFullyQualifiedName, t)
            }
          } else {
            // Object type
            // For now, we only consider null check
            val name = entry._1.getFullyQualifiedName()
            repairNullness(name)
          }
        }
      }
    }

    if(Options.defectClasses.contains("methodCalls")) {
      val methodCalls = new MethodCallRunTime(ast, fault)
      if(Options.methodCallRepairType.contains(S3Properties.MethodCallRepType.CURRENT_CLASS)){
        methodCalls.methodsInCurrentClass(envIDs, envVals)
      }

      if(Options.methodCallRepairType.contains(S3Properties.MethodCallRepType.USED_METHOD_CALLS)){
        methodCalls.usedMethods(envIDs, envVals)
      }

      if(Options.methodCallRepairType.contains(S3Properties.MethodCallRepType.ENCODING_ONLY)){
        methodCalls.encodingOnly(envIDs, envVals)
      }

      if(Options.methodCallRepairType.contains(S3Properties.MethodCallRepType.USED_METH_1_PARAM)){
        methodCalls.usedMethodCallsRep1Param(envIDs, envVals)
      }

      // Method call used at fault line, find if these method names are used elsewhere with differrent params
      if(Options.methodCallRepairType.contains(S3Properties.MethodCallRepType.USED_METH_ELSE_WHERE)){
        methodCalls.methAtFaultUsedElseWhere(envIDs, envVals, surroundingClass)
      }

      if(Options.methodCallRepairType.contains(S3Properties.MethodCallRepType.METH_IN_USED_VAR)){
        methodCalls.methInUsedVar(envIDs, envVals)
      }
    }
    val rewriter = getRewriterUptoDate(fault.getFileName())
    if(fault.getJavaNode().isInstanceOf[Statement]){
      // Statement level instrumentation
      val choose: MethodInvocation = InstrumentUtils.createChooseNode("angelixChooseBool", fault, envVals, envIDs)
      val guardedStmt = InstrumentUtils.createGuardNode(choose, fault.getJavaNode().asInstanceOf[Statement])
      modifiedRewriter.put(fault.getFileName(), replaceNode(rewriter, fault.getJavaNode(), guardedStmt))
      logger.debug("Instrumented AngelixChoose: " + guardedStmt)
    }else {
      // Expression level instrumentation
      // Note that this cast is because here we operate on expression only. Do this, we can get type binding
      val faultyExp: Expression = (faultNode.asInstanceOf[Expression])
      val faultyExpType: String = faultyExp.resolveTypeBinding.toString
      var chooseMethod: String = null
      if (frontend.Repairable.isIntType(faultyExpType)) chooseMethod = "angelixChooseInt"
      else if (frontend.Repairable.isBoolType(faultyExpType)) chooseMethod = "angelixChooseBool"
      else if (frontend.Repairable.isDoubleType(faultyExpType)) chooseMethod = "angelixChooseDouble"
      else if (frontend.Repairable.isCharType(faultyExpType)) chooseMethod = "angelixChooseChar"
      else if (frontend.Repairable.isFloatType(faultyExpType)) chooseMethod = "angelixChooseFloat"
      else {
        throw new RuntimeException("Need to have method for choosing type: " + faultyExpType)
      }

      val choose: MethodInvocation = InstrumentUtils.createChooseNode(chooseMethod, fault, envVals, envIDs)
      modifiedRewriter.put(fault.getFileName(), replaceNode(rewriter, fault.getJavaNode(), choose))
      logger.debug("Instrumented AngelixChoose: " + choose)
    }
  }

  private def varNotFromCurrentMethod(fault: FaultIdentifier, v: JDTNameWrapper): Boolean = {
    !fault.getLocalVisibleVars().containsKey(v) &&
    !fault.getStaticFieldVars().containsKey(v) && !fault.getNonStaticFieldVars().containsKey(v)
  }

  def applyChange(fileName: String, toBeRepNode: ASTNode, repNode: ASTNode): Unit = {
    val rewriter = getRewriterUptoDate(fileName)
    if(toBeRepNode.isInstanceOf[Statement]){
      // Statement level. We apply guard
      val guardStmt = toBeRepNode.getAST.newIfStatement()
      guardStmt.setExpression(ASTNode.copySubtree(guardStmt.getAST(),repNode).asInstanceOf[Expression])
      guardStmt.setThenStatement(ASTNode.copySubtree(guardStmt.getAST(), toBeRepNode).asInstanceOf[Statement])
      modifiedRewriter.put(fileName, replaceNode(rewriter, toBeRepNode, guardStmt))
    }else {
      // Expression level
      modifiedRewriter.put(fileName, replaceNode(rewriter, toBeRepNode, repNode))
    }
  }

  private def getModifiedRewriterContent(): mutable.LinkedHashMap[String, String] = {
    if(modifiedRewriterContent.isEmpty) {
      if (modifiedRewriter.size == 0)
        throw new RuntimeException("Attempting to get content of empty rewriters!");

      val iter = modifiedRewriter.iterator
      while (iter.hasNext) {
        val ent = iter.next()
        //ASTRewrite originalRewriter = mapOriginalRewriter.get(ent.getKey());//key is fileName
        val originalContent = JavaParser.modifyingFiles.getOrElse(ent._1, null)
        //System.out.println(JavaParser.modifyingFiles());
        val modifiedDoc = new org.eclipse.jface.text.Document(originalContent)
        val rewriter =  modifiedRewriter.getOrElse(ent._1, null)
        val edits = rewriter.rewriteAST(modifiedDoc, null)
        edits.apply(modifiedDoc)
        val modifiedContent = modifiedDoc.get()
        modifiedRewriterContent.put(ent._1, modifiedContent)
      }
    }
    return modifiedRewriterContent
  }

  def classPaths() = {
    val bytecodeOutput: String = utils.FileFolderUtils.getInstrumentedDirWithPrefix()
    val variantOutputFile: File = new File(bytecodeOutput)
    var bc: Array[URL] = null
    val originalURL: Array[URL] = utils.FileFolderUtils.getURLforInstrumented()
    bc = utils.FileFolderUtils.redefineURL(variantOutputFile, originalURL)
    bc
  }

  def classPaths2String(separator:String) = {
    val cl = classPaths()
    cl.foldLeft(""){(res, p) => res+p+separator}
  }

  def compile(): Unit ={
    val bc = classPaths()
    val classPathString = bc.foldLeft(new java.util.ArrayList[String]()){
      (res, path) =>{
        res.add(path.toString)
        res
      }
    }

    val path2InstrumentedClassFiles = utils.FileFolderUtils.getInstrumentedDirWithPrefix()
    import scala.collection.JavaConversions._
    val fileSourceMap = getModifiedRewriterContent
    if(Options.writeOutput2Disk){
      val angelixFolder = System.getProperty(S3Properties.ANGELIX_FOLDER)
      val outFolder = angelixFolder+File.separator+"output"
      new File(outFolder).delete()
      fileSourceMap.foreach( f=> {
        //TODO: we now only care about file name, later care about fully qualified name, for multi files
        val sp = f._1.split("\\.")
        val outFolderFinal = sp.take(sp.length-1).foldLeft(outFolder){
          (res, fd) => res+File.separator+fd
        }
        new File(outFolderFinal).mkdirs()
        val fName = sp.last
        mylib.Lib.writeText2File(f._2.replace("import backend.RunTime;\n","").replace("import backend.TypeBox;\n",""), new File(outFolderFinal+File.separator+fName+".java"))
      })
    }
    val compiler: JavaJDKCompiler = new JavaJDKCompiler(path2InstrumentedClassFiles,classPathString,fileSourceMap, null, null)
    val status=compiler.compile()
    if(status != JavaJDKCompiler.Status.COMPILED) {
      throw new RuntimeException("!!! Instrumentation does not compile...")
    }else{
      logger.info("Instrumentation Completed!")
    }
  }
}
object BackEndInstrument{
  def classPaths() = {
    val bytecodeOutput: String = utils.FileFolderUtils.getInstrumentedDirWithPrefix()
    val variantOutputFile: File = new File(bytecodeOutput)
    var bc: Array[URL] = null
    val originalURL: Array[URL] = utils.FileFolderUtils.getURLforInstrumented()
    bc = utils.FileFolderUtils.redefineURL(variantOutputFile, originalURL)
    bc
  }

  def classPaths2String(separator:String) = {
    val cl = classPaths()
    cl.foldLeft(""){(res, p) => res+p.toString().split(":")(1)+separator}
  }
}