package backend
import java.io._

import angelic.AngelicFix.AngelicValue
import driver.{ConfigurationProperties, Options, S3Properties}
//import engines.RepairDriver
import net.liftweb.json.JsonDSL._
import net.liftweb.json.Serialization._
import net.liftweb.json._
import org.apache.log4j.Logger
import utils.{FileFolderUtils, ProcessExecutor}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Created by dxble on 9/9/16.
  */
abstract class Synthesizer(angelixFolder: String) {
  val logger = Logger.getLogger(this.getClass)

  lazy val angelixForestFile = angelixFolder+File.separator+"last-angelic-forest.json"
  lazy val extractedDir = angelixFolder+File.separator+"extracted"

  lazy val configFile = angelixFolder+File.separator+"config.json"
  lazy val additionalConfigFile = angelixFolder+File.separator+"addconfig.prop"
  lazy val patchFile = angelixFolder+File.separator+"synthesized.patch"
  lazy val levels = Options.synthesisLevels.split(";")

  protected def forest2Json(angelicForest: mutable.HashMap[String, ArrayBuffer[Iterable[AngelicValue]]]): String = {
    var index = 0
    angelicForest.foldLeft(""){
      (res, p) => {
        // p._1 is test number (id),
        // p._2 is the angelic paths for that test
        implicit val formats = DefaultFormats
        val jsonString = write(p._2)
        if(index < angelicForest.size -1 ){
          index += 1
          res + "\""+p._1+"\"" +":"+jsonString+","+"\n"
        }else{
          res + "\""+p._1+"\"" +":"+jsonString+"\n"
        }
      }
    }
  }

  def writeAngelicForest2File(angelicForest: mutable.HashMap[String, ArrayBuffer[Iterable[AngelicValue]]]) = {
    val forestJSon = forest2Json(angelicForest)
    FileFolderUtils.write2File(angelixForestFile, "{\n" + forestJSon + "\n}")
  }

  /**
    * Loop a certain times according to synthesis levels
    *
    * @return true if finally succeed, false otherwise
    */
  def synthesizeFix(additionalConfig: String): Boolean = {
    FileFolderUtils.write2File(additionalConfigFile, additionalConfig)
    levels.foreach(synthLevel => {
      if(Options.synthesisEngine == S3Properties.SynthesisEngine.SYNTHESIS_ANGELIX_MAXSAT)
        logger.info("Doing synthesis with level: "+synthLevel)
      else
        logger.info("Meta solver config: "+additionalConfig)

      val config = configWithSynthLevel(synthLevel)
      val configJson = prettyRender(config)
      FileFolderUtils.write2File(configFile, configJson)

      try {
        val success = callSynthesizer()
        if(success)
          return true
      }catch {
        case e: Exception => {
          logger.info(e.getMessage)
          logger.info("Continue after synthesis exception...")
        }
      }
    })
    return false
  }

  /**
    *
    * @return true if synthesis success, otherwise false
    */
  protected def callSynthesizer(): Boolean

  def configWithSynthLevel(synthLevel: String) = {
    var synthTimeout = Options.synthesisTimeout
    if(Options.synthesisEngine == S3Properties.SynthesisEngine.SYNTHESIS_ANGELIX_MAXSAT)
      synthTimeout = synthTimeout * 1000 // convert from second to millisecond
    val config =
      ("encodingConfig" ->
        ("componentsMultipleOccurrences" -> true) ~
          // better if false, if not enough primitive components, synthesis can fail
          ("phantomComponents" -> true) ~
          ("repairBooleanConst" -> false) ~
          ("repairIntegerConst" -> false) ~
          ("level" -> Options.synthesisConfigLevel)
        ) ~
        ("simplification" -> Options.synthesisSimplification) ~
        ("reuseStructure" -> true) ~
        ("spaceReduction" -> Options.spaceReduction) ~
        ("componentLevel" -> synthLevel.trim) ~
        ("solverBound" -> Options.synthesisSolverBound) ~
        ("solverTimeout" -> synthTimeout)
    config
  }
}
case class SynthesisExecutor(angelixFolder: String) extends Synthesizer(angelixFolder){

  override def callSynthesizer(): Boolean = {
    logger.info("Calling Synthesizer: "+Options.synthesisEngine)
    val jarFile = Options.synthesisJar
    val additionalOpt = {
      if(Options.synthesisEngine == S3Properties.SynthesisEngine.SYNTHESIS_META)
        " Meta x y "+additionalConfigFile
      else
      if(Options.synthesisEngine == S3Properties.SynthesisEngine.SYNTHESIS_ANGELIX_MAXSAT)
        ""
      else if(Options.synthesisEngine == S3Properties.SynthesisEngine.SYNTHESIS_CVC4)
        " CVC4 "+Options.solverPath + " "+Options.beautifierPath+" "+additionalConfigFile
      else if(Options.synthesisEngine == S3Properties.SynthesisEngine.SYNTHESIS_ENUM)
        " Enum "+Options.solverPath + " "+Options.beautifierPath+" "+additionalConfigFile
    }

    //TODO: set timeout for process
    import scala.sys.process._
    logger.info("Calling java at: " + System.getProperty("java.home"))

    val execSynth = System.getProperty("java.home")+"/bin/java" + " -jar " + jarFile + " " + angelixForestFile + " " + extractedDir + " " + patchFile + " " + configFile + additionalOpt
    val res = execSynth.!!
    if(res.contains("SUCCESS")) {
      logger.info("SUCCESS")
      return true
    }
    else {
      if(res.contains("TIMEOUT"))
        logger.info("TIMEOUT")
      else if(res.contains("FAIL"))
        logger.info("FAIL")
      else logger.info("UNKNOWN")
      return false
    }
  }
}
/*case class SynthesisExecWithTimeout(angelixFolder: String) extends Synthesizer(angelixFolder){

  import scala.actors._
  import scala.actors.Actor._

  private val caller = self

  private val reader = actor {
    println("created actor: " + Thread.currentThread)
    var continue = true
    loopWhile(continue){
      reactWithin(Options.synthesisTimeout * 1000) {
        case TIMEOUT => {
          caller ! "react timeout"
          continue = false
        }
        case proc:Process =>
          println("entering first actor " + Thread.currentThread)
          val streamReader = new java.io.InputStreamReader(proc.getInputStream)
          val bufferedReader = new java.io.BufferedReader(streamReader)
          val stringBuilder = new java.lang.StringBuilder()
          var line:String = null
          while({line = bufferedReader.readLine; line != null}){
            stringBuilder.append(line)
            stringBuilder.append("\n")
          }
          bufferedReader.close
          caller ! stringBuilder.toString
          continue = false
      }
    }
  }

  def run(command:String): Boolean = {
    println("gonna runa a command: " + Thread.currentThread)
    val args = command.split(" ")
    val processBuilder = new ProcessBuilder(args: _* )
    processBuilder.redirectErrorStream(true)
    val proc = processBuilder.start()

    //Send the proc to the actor, to extract the console output.
    reader ! proc

    //Receive the console output from the actor.
    receiveWithin(Options.synthesisTimeout * 1000) {
      case TIMEOUT => {
        logger.info("Synthesis Timeout")
        false
      }
      case result:String => {
        logger.debug(result)
        if(result.contains("SUCCESS")) true
        else false
      }
    }
  }


  override def callSynthesizer(): Boolean = {
    val jarFile = Options.synthesisJar
    val additionalOpt = {
      if(Options.synthesisEngine == Properties.SynthesisEngine.SYNTHESIS_ANGELIX_MAXSAT)
        ""
      else
        " Meta x y z"
    }
    val execSynth = "java -jar " + jarFile + " " + angelixForestFile + " " + extractedDir + " " + patchFile + configFile +" "+additionalOpt
    run(execSynth)
  }
}*/
// TODO: later implement this to call to synthesis engines directly rather than using native execution
// This is not yet properly implemented yet due to dependency hell :(
/*case class SynthesisEngines(angelixFolder: String) extends Synthesizer(angelixFolder){
  override def callSynthesizer(): Boolean = {
    val jarFile = Options.synthesisJar
    val additionalOpt = {
      if(Options.synthesisEngine == Properties.SynthesisEngine.SYNTHESIS_ANGELIX_MAXSAT)
        ""
      else
        " Meta x y z"
    }
    RepairDriver.main(Array[String](angelixForestFile, extractedDir, patchFile, configFile, "Meta","x","y","z"))
    RepairDriver.result
  }
}*/
