package backend

import org.apache.log4j.Logger

import driver.Options
import org.eclipse.jdt.core.dom._
import testrunner.{PositiveTest, TestCase}
import parser.javaparser.JavaParser
import java.io.File
import java.net.URL

import junithandler.compiler.JavaJDKCompiler
import org.eclipse.jface.text.{BadLocationException, Document}
import org.eclipse.text.edits.{MalformedTreeException, TextEdit}
import org.eclipse.jdt.core.dom.rewrite.{ASTRewrite, ListRewrite}
import utils.InstrumentUtils
/**
  * Created by dxble on 9/1/16.
  */
case class TestDriverNotCompileException(message: String) extends Exception(message)

class UnitTestDriver {
  val logger = Logger.getLogger(this.getClass)

  var driverCU: CompilationUnit = null
  var content: String = null
  var newContent: String = null
  var driverPackage: String = null

  def main(args: Array[String]) {
    val test = PositiveTest("AssignmentTest#evaluatesExpression2", 0)
    //test.setPackageName("tester1")
    createTestDriver(test)
    compile()
  }

  // Create a test driver for a test accordingly, in order for
  // jpf-symbc to run symbolic execution on that test for spec inference
  def createTestDriver(test: TestCase[Any]): Unit = {
    if (driverCU == null) {
      val path2TestCaller = Options.angelixRoot + File.separator + "src/main/java/backend/CustomUnitTestCaller.java"
      val sourceFolder = Options.angelixRoot + File.separator + "src/main/java/backend/"
      val unitName = "backend.CustomUnitTestCaller"
      val temp = JavaParser.parseOneFile(path2TestCaller, sourceFolder, unitName)
      driverCU = temp._1
      content = temp._2
    }

    val original: ASTRewrite = ASTRewrite.create(driverCU.getAST)

    val pkg = driverCU.getPackage

    // Importing the test class
    val testQualifiedName = test.classFullName
    if(test.packageName != null) {
      val ip = InstrumentUtils.createImport(driverCU, testQualifiedName)
      val lrw = original.getListRewrite(driverCU, CompilationUnit.IMPORTS_PROPERTY)
      lrw.insertLast(ip, null)

      val newPkg = driverCU.getAST.newPackageDeclaration()
      val pkgName = test.packageName
      newPkg.setName(newPkg.getAST().newName(pkgName))
      original.replace(pkg, newPkg, null)
      driverPackage = pkgName
    }else{
      original.remove(pkg, null)
    }

    val testerInstance = InstrumentUtils.createTesterInstance(driverCU, test.className, test.classFullName, test.hasConstructor, test.numArgs)
    val methodCall = InstrumentUtils.createMethodCallNoArgs(driverCU, test.methodName, "tester")
    val rewrite: ListRewrite = original.getListRewrite(driverCU.types().get(0).asInstanceOf[TypeDeclaration].getMethods()(0).getBody, Block.STATEMENTS_PROPERTY)
    rewrite.insertFirst(driverCU.getAST.newExpressionStatement(methodCall), null)
    if(test.hasSetUp){
      val setupCall = InstrumentUtils.createMethodCallNoArgs(driverCU, "setUp" , "tester")
      rewrite.insertFirst(driverCU.getAST.newExpressionStatement(setupCall), null)
    }

    // Now testing before and before class
    Options.testSetUpMethods.foreach(m => {
      val bef = InstrumentUtils.createMethodCallNoArgs(driverCU, m , "tester")
      rewrite.insertFirst(driverCU.getAST.newExpressionStatement(bef), null)
    })

    rewrite.insertFirst(driverCU.getAST.newExpressionStatement(InstrumentUtils.createPrint(driverCU, "Creating and running test driver...")), null)
    rewrite.insertFirst(driverCU.getAST.newExpressionStatement(testerInstance), null)
    contentOfInstrumented(rewrite)
  }

  private def contentOfInstrumented(rewrite: ListRewrite): Unit = {
    val document: Document = new Document(content)
    val edits: TextEdit = rewrite.getASTRewrite.rewriteAST(document, null)
    try {
      edits.apply(document)
      val newSource: String = document.get
      //println("Writing to document: "+newSource)
      //Files.write(Paths.get(filePath),newSource.getBytes(StandardCharsets.UTF_8))
      logger.debug("Instrumented test driver: \n"+newSource)
      newContent = newSource
    }
    catch {
      case e: MalformedTreeException => {
        e.printStackTrace
      }
      case e: BadLocationException => {
        e.printStackTrace
      }
    }
  }

  def classPaths() = {
    val bytecodeOutput: String = utils.FileFolderUtils.getInstrumentedDirWithPrefix()
    val variantOutputFile: File = new File(bytecodeOutput)
    var bc: Array[URL] = null
    val originalURL: Array[URL] = utils.FileFolderUtils.getURLforInstrumented()
    bc = utils.FileFolderUtils.redefineURL(variantOutputFile, originalURL)
    bc
  }

  def compile(): Unit = {
    val bc = classPaths()
    val str = new java.util.ArrayList[String]()
    //str.add("/usr/lib/jvm/java-8-oracle/jre/lib/rt.jar")
    val classPathString = bc.foldLeft(str) {
      (res, path) => {
        res.add(path.toString)
        res
      }
    }

    val path2InstrumentedClassFiles = utils.FileFolderUtils.getInstrumentedDirWithPrefix()
    import scala.collection.JavaConversions._
    val fileSourceMap = new scala.collection.mutable.LinkedHashMap[String, String]
    fileSourceMap.put("backend.CustomUnitTestCaller", newContent)
    val compiler: JavaJDKCompiler = new JavaJDKCompiler(path2InstrumentedClassFiles,classPathString, fileSourceMap, null, null)
    val status = compiler.compile()
    if (status != JavaJDKCompiler.Status.COMPILED) {
      throw new TestDriverNotCompileException("!!! Instrumentation does not compile...")
    } else {
      logger.info("UnitTestDriver Instrumentation Completed!")
    }
  }
}






