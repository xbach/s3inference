package driver

import parser.javaparser.JavaParser
import java.io.{File, OutputStream, PrintStream}
import java.nio.file.Paths

import net.liftweb.json.Serialization.write
import angelic.AngelicFix.{AngelicForest, AngelicPath, AngelicValue}
import backend._
import localization.{FaultIdentifier, Localization, Reducer, Trace}
import frontend.{Dumper, FrontEndInstrumentVisitor, Golden, WriteFault2SMT}
import backend.Inference
import me.tongfei.progressbar.ProgressBar
import testrunner.{JUnitTestInvoker, NegativeTest, PositiveTest, TestCase}

import scala.collection.mutable.ArrayBuffer
import org.apache.log4j.{BasicConfigurator, Logger}
import utils.analysis.PrioritizeSearchSpace
import utils.{ASTUtils, FileFolderUtils, VariableCoOccurrence}

import scala.collection.mutable

/**
  * Created by dxble on 8/27/16.
  */
object Main {
  val logger = Logger.getLogger(this.getClass)

  def example_settings() ={
    //driver.Options.homeFolder="/home/dxble/workspace/jangelix/"

    // Note: we must specify the following folders:
    //  1. sourceFolder: the folder of the source of the software under test (SUT). For parsing of source code.
    //  2. testFolder: the folder of the source of tests of the SUT. For parsing of tests,
    //     e.g. to determine public test methods, etc
    //  3. junitDeps: junit jar and hamcrest jar to run tests
    //  4. jpfDeps: to run symbolic execution
    driver.Options.sourceFolder="/home/dxble/workspace/testJAngelix/assignment/src/main/"
    driver.Options.testFolder="/home/dxble/workspace/testJAngelix/assignment/src/test/"
    driver.Options.junitDeps="/home/dxble/workspace/jangelix/lib/junit-4.12.jar;/home/dxble/workspace/jangelix/lib/hamcrest-core-1.3.jar"
    driver.Options.jpfDeps="/home/dxble/workspace/jpf/jpf-core/build;/home/dxble/workspace/jpf/jpf-symbc/build"

    // Note: dependencies must include:
    //  1. the java source code of jangelix because we need it for instrumentation to compile
    //  2. the test folder of the software under test because we need to run the test.
    //     The test folder could be either testFolder if the test is compiled together with the application,
    //     or testClassDir if the test is already compiled before hand.
    //     To compile test, we need to include backend.RunTime into the test folder
    //  3. junit dependencies and jpf dependencies
    // We must specify any localLibs if require. LocalLibs are libs that are required by SUT

    driver.Options.testClassDir="/home/dxble/workspace/testJAngelix/assignment/build/test/"
    //val angelixLib = "/home/dxble/workspace/jangelix/lib/"
    driver.Options.dependencies="/home/dxble/workspace/jangelix/src/main/java;/home/dxble/workspace/jangelix/src/main/scala/;"+driver.Options.testClassDir+";"+driver.Options.junitDeps+";"+driver.Options.jpfDeps
    driver.Options.localLibs=driver.Options.jpfDeps

    //driver.Options.appClassDir="build/main/"

    driver.Options.angelixRoot="/home/dxble/workspace/jangelix"
    driver.Options.faultFile="Assignment"
    driver.Options.synthesisLevels="alternatives"
    driver.Options.synthesisJar="/home/dxble/workspace/repairtools/angelix/src/synthesis/target/scala-2.10/repair-maxsat-assembly-1.0.jar"

    driver.Options.assertFile="/home/dxble/workspace/testJAngelix/assignment/assert.json"
    // Note: note that the assert file of failing test should follow the order
    // for example: [1] is index of AssignmentTest#evaluatesExpression -- the failing test as indicated bellow
    //              [2] is index of another passing test, similarly for [3]

    driver.Options.failingTests="AssignmentTest#evaluatesExpression"
  }

  def dumpExpectedOutput(workingDir: String, assertFile: String): Unit = {
    //import scala.sys.process._
    //val exec = "python " + driver.Options.angelixRoot+ File.separator+ "Dumper.py "+workingDir+ " "+ assertFile
    //exec.! //lines_! ProcessLogger(line => println(line))
    val dumper = new Dumper(workingDir, assertFile)
    dumper.dumpExpectedValues()
  }

  var configFile: String = ""
  def main(args: Array[String]) {
    //org.apache.log4j.BasicConfigurator.configure()
    //logger.setAdditivity(false)

    val startTime = System.currentTimeMillis()
    // *** Some settings ****
    logger.debug("*************************************************************************\n"+
             "\t\t\t\t\t\t\t\t\t\t* REMINDER: Set your assert file correctly to capture correct output.\n"+
            "\t\t\t\t\t\t\t\t\t  *************************************************************************")
    //example_settings()
    //Options.configFile = args(0)
    //println(Options.configFileX)
    configFile = args(0)
    ConfigurationProperties.load(args(0))

    ConfigurationProperties.loadRequiredProps()
    if(Options.debug)
      ConfigurationProperties.print()

    val workingDir = Paths.get(".").toAbsolutePath().normalize().toString()
    logger.info("Working dir: "+workingDir)
    val angelixFolder = new File(workingDir + File.separator+ "angelix")
    mylib.Lib.removeDirectory(angelixFolder)
    angelixFolder.mkdir()
    System.setProperty(S3Properties.ANGELIX_FOLDER, angelixFolder.getCanonicalPath)
    // *** Ended settings ***

    // *** Dumping expected output ***
    if(Options.assertFile != null && Options.assertFile != "")
      dumpExpectedOutput(angelixFolder.getCanonicalPath, driver.Options.assertFile)
    // *** End Dumping ***

    JavaParser.batchParsingInDir(driver.Options.sourceFolder)
    logger.info("Parsing Completed!")
    logger.info("FrontEnd: Instrumenting for trace and dump...")
    val iter = JavaParser.modifyingFiles.iterator
    while (iter.hasNext){
      val next = iter.next()
      if(!Options.skipConfigValidation && Options.frontEndInstrumentation) {
        val visitor = new FrontEndInstrumentVisitor(next._1)
        val ast = JavaParser.globalASTs.getOrElse(next._1, null)
        ast.accept(visitor)
      }

      /*val variableCoOccurrence = new VariableCoOccurrence(next._1)
      ast.accept(variableCoOccurrence)
      println("Debug variable co-occurrence")*/
    }

    System.setProperty(S3Properties.SYMBOLIC_RUNTIME, "false")
    if(!Options.skipConfigValidation && Options.frontEndInstrumentation)
      FrontEndInstrumentVisitor.compile()
    if(Options.frontendInstrumentOnly) {
      logger.info("Completed frontend instrumentation only.")
      sys.exit(0)
    }
    val testInvoker = new JUnitTestInvoker
    if(Options.test2Debug != null){
      val test = testInvoker.findTestWithName(Options.test2Debug)
      println(testInvoker.invokeOneTestFresh(test, true, true, FrontEndInstrumentVisitor.classPaths()))
      sys.exit(0)
    }

    if(Options.applyFixRetestOnly){
      // Note Options.faultFile here is only allowed one file for now
      PatchTester.applyPatch(Options.faultFile, Options.patchFile)
      val (passAll, failedTests) = testInvoker.invokeAllTests(false, false, false, FrontEndInstrumentVisitor.classPaths())
      if(passAll){
        println("SUCCESSALL")
      }else{
        failedTests.foreach(ft => println("FailedTest:"+ft))
      }
      sys.exit(0)
    }

    if(!Options.skipConfigValidation) {
      logger.info("FrontEnd: Running Tests to collect trace and dump expected output of positive tests...")
      testInvoker.invokeAllTests(true, true, true, FrontEndInstrumentVisitor.classPaths())
      logger.info("FrontEnd: Running Tests Completed!")
    }else{
      logger.info("Skipping configuration validation: e.g., validate tests, dump, trace")
    }

    if(Golden.isGoldenSetUp()){
      logger.info("Golden: Running Negative Tests for Dumping")
      val goldenNeg = testInvoker.goldenNegativeTestName()
      val pb: ProgressBar = new ProgressBar("Golden Dumping Negative Tests", goldenNeg.size)
      pb.start()
      val (passAll, failedTests) = testInvoker.invokeTests(false, true, false, goldenNeg, pb, Golden.classPaths())
      if(!passAll)
        throw new RuntimeException("Golden version tests failed! Failed tests: "+failedTests)
      pb.stop()
      logger.info("Golden: Dumping completed!")
    }

    val localization = new Localization(testInvoker.negTests, testInvoker.posTests)
    val faultGroups = localization.doLocalization()
    logger.debug(faultGroups.foldLeft(""){(res, g) => res+"\nFault Group: "+g.foldLeft(""){(res1,e) => res1+e+"; "}})
    //println(faultGroups)

    var repaired = false
    var faultIndex = 0
    val allTests = new ArrayBuffer[TestCase[Any]]
    allTests.appendAll(testInvoker.negTests)
    allTests.appendAll(testInvoker.posTests)

    System.setProperty(S3Properties.SYMBOLIC_RUNTIME, "true")
    System.setProperty(S3Properties.ANGELIX_DUMP, "false")

    WriteFault2SMT.writeOriginalFault2Extracted(faultGroups)
    // val angelicForestFile = angelixFolder.getCanonicalPath+File.separator+"last-angelic-forest.json"
    // val solvedPCFile = new File(FileFolderUtils.getInstrumentedDirWithPrefix()+ File.separator + "solvedPC.txt")

    val synthesizer = new SynthesisExecutor(angelixFolder.getCanonicalPath)
    //val synthesizer = new SynthesisExecWithTimeout(angelixFolder.getCanonicalPath)

    val patchCombiner = new CombineAllPatch(angelixFolder.getCanonicalPath, testInvoker.negTests.toArray, allTests)

    while (!repaired && faultIndex < faultGroups.size){
      val expressions = faultGroups(faultIndex)
      logger.info("Chosen fault: "+expressions)

      // ==> Reduce tests for inference
      val (reducedNegTests, reducedPostTests) = Reducer.reduce(allTests, localization.positiveTraces, localization.negativeTraces, expressions)

      logger.info("Instrumenting angelixChoose at chosen faults...")
      val backendInstr = new BackEndInstrument(expressions)
      backendInstr.instrumentChosenLocations()
      backendInstr.compile()

      val installedSymbLocations = expressions.foldLeft(new java.util.ArrayList[String]())((res, e) => {
        res.add(e.getFaultLocString())
        res
      })

      // ==> Specification Inference
      val angelicForest = Inference.inferSpec(reducedNegTests, reducedPostTests, installedSymbLocations, angelixFolder.getAbsolutePath)

      if(angelicForest.size > 0) {
        synthesizer.writeAngelicForest2File(angelicForest)
        val additionalConf = additionalConfig(angelixFolder.getAbsolutePath, expressions)
        def synthesisCustimzedSpaces(additionalConf: String): Unit = {
          val success = synthesizer.synthesizeFix(additionalConf)

          if (success) {
            logger.info("Applying patch for retesting...")
            // TODO: for now we are assuming that we fix only one file
            val file2Patch = expressions(0).getFileName()
            val patchFile = angelixFolder.getCanonicalPath + File.separator + "synthesized.patch"
            PatchTester.applyPatch(file2Patch, patchFile)

            logger.info("Now retesting...")
            val (passAll, failedTests) = {
              if (Options.retestSubset)
                testInvoker.invokeTestsFresh(false, false, false, Trace.testsHavingTrace, backendInstr.classPaths())
              else
                testInvoker.invokeAllTests(false, false, false, backendInstr.classPaths())
            }
            logger.info("Done retesting...")
            if (passAll) {
              repaired = true
            } else {
              // Copy patch file to folder named tested. File name is renamed to the [faultIndex].patch
              val testedPatchFolder = angelixFolder.getCanonicalPath + File.separator + "tested"
              new File(testedPatchFolder).mkdir()
              val testedPatchFile = testedPatchFolder + File.separator + faultIndex + ".patch"
              new File(patchFile).renameTo(new File(testedPatchFile))
              patchCombiner.addPatchTest(testedPatchFile, failedTests)

              logger.info("Retesting found: number of failed tests: " + failedTests.size)
              new File(angelixFolder.getAbsolutePath + File.separator + "output").delete()
              failedTests.foreach(t => {
                logger.info("Failed test: " + t)
              })
            }
          }
        }
        synthesisCustimzedSpaces(additionalConf)
        if(!repaired){
          val incremental = additionalConfigIncrementalSynth(angelixFolder.getAbsolutePath, expressions)
          synthesisCustimzedSpaces(incremental)
        }

      }else{
        logger.info("Found no angelic forest for faults at: "+expressions)
      }

      // Clean generated type file
      if(!repaired)
        new File(angelixFolder.getAbsolutePath+File.separator+driver.S3Properties.ACTUAL_VAR_TYPES_FILE).delete()
      faultIndex += 1
    }

    if(repaired){
      logger.info("Found repair!")
    }else{
      if(Options.combinePatchIfFail && faultGroups.size >0){
        logger.info("Combining patch...")
        val combinedPatches = patchCombiner.combine()
        // TODO: for now we only allow patching one faulty file
        val file2Patch = faultGroups(0)(0).getFileName()
        combinedPatches.foreach(p =>{
          val classPaths = PatchTester.applyPatch(file2Patch, p)
          logger.info("Now retesting...")
          val (passAll, failedTests) = testInvoker.invokeAllTests(false, false, false, classPaths)
          logger.info("Done retesting...")
          if (passAll) {
            repaired = true
            logger.info("Found repair at: "+p)
            logger.info("Copying patch file to angelixFolder: synthesized.patch")
            new File(p).renameTo(new File(angelixFolder + File.separator + "synthesized.patch"))
          }else {
            new File(angelixFolder.getAbsolutePath+File.separator+"output").delete()
            logger.info("Retesting found: number of failed tests: " + failedTests.size)
            failedTests.foreach(t => {
              logger.info("Failed test: " + t)
            })
          }
        })
      }
      if(repaired)
        logger.info("Found repair!")
      else logger.info("Have not found any repair...!")
    }
    // We dump the method call with its mangled name. This is because during synthesis, we view method call as a variable,
    // so each method call's concrete run is mangled to a variable name.
    NameMangling.dumpMangledMethodCall(new File(angelixFolder.getAbsolutePath+ File.separator + S3Properties.MANGLED_METHOD_CALL_FILE))
    NameMangling.dumpMangledNullCheck(new File(angelixFolder.getAbsolutePath+ File.separator + S3Properties.MANGLED_NULL_CHECK_FILE))
    //NameMangling.dumpMangledVar(new File(angelixFolder.getAbsolutePath+ File.separator + Properties.MANGLED_VAR_FILE))
    val endTime = System.currentTimeMillis()
    logger.info("Running time: "+ (endTime - startTime)/1000.0)
    sys.exit(0)
  }

  def additionalConfigIncrementalSynth(angelixFolder: String, faultGroup: ArrayBuffer[FaultIdentifier]) = {
    var cf = ""
    if (ConfigurationProperties.hasProperty("feature.syntax"))
      cf += "feature.syntax=" + ConfigurationProperties.getProperty("feature.syntax") + "\n"
    if (ConfigurationProperties.hasProperty("feature.semantic"))
      cf += "feature.semantic=" + ConfigurationProperties.getProperty("feature.semantic") + "\n"
    cf += "actualVarTypeFile=" + angelixFolder + File.separator + S3Properties.ACTUAL_VAR_TYPES_FILE + "\n"
    cf
  }

  def additionalConfig(angelixFolder: String, faultGroup: ArrayBuffer[FaultIdentifier]) = {
    var cf = ""
    if(ConfigurationProperties.hasProperty("feature.syntax"))
      cf += "feature.syntax="+ConfigurationProperties.getProperty("feature.syntax")+"\n"
    if(ConfigurationProperties.hasProperty("feature.semantic"))
      cf += "feature.semantic="+ConfigurationProperties.getProperty("feature.semantic")+"\n"
    cf += "actualVarTypeFile="+angelixFolder+File.separator+S3Properties.ACTUAL_VAR_TYPES_FILE+"\n"
    //TODO: separate levels for each fault
    if(Options.prioritizeSpace){
      val prio = new PrioritizeSearchSpace()
      prio.analyzeSurroundingContext(faultGroup)
      val spaces = prio.prioritizedSpace
      if(!spaces.values.exists(sp => sp.isEmpty)){
        val levels = spaces.values.flatten.foldLeft(new ArrayBuffer[String]()){
          (res,s) => {
            if(!res.contains(s.toString))
              res.append(s.toString)
            res
          }
        }
        val allLevelsPrioritized = levels.mkString(";")
        logger.info("Prioritized Search Space: "+allLevelsPrioritized)
        if(!levels.isEmpty)
          cf += "synthesisLevels="+allLevelsPrioritized
        //cf += "synthesisLevels="+spaces.values.mkString(";").replace("Queue(","").replace(")","").replace(" ","").replace(",",";")
      }else{
        logger.info("Prioritized Search Space: "+spaces)
      }
    }
    cf
  }
}
