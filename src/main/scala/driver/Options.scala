package driver

/**
  * Created by dxble on 8/27/16.
  */

import java.io.File
import java.util

import driver.S3Properties.{MethodCallRepType, TestRunnerType}
//import myLib.MyjavaLib
import utils.FileFolderUtils

import scala.collection.mutable
import scala.collection.immutable
import scala.collection.mutable.ArrayBuffer

object Options {
  lazy val testRunner: S3Properties.TestRunnerType.TRType = {
    val prop = ConfigurationProperties.getProperty("testRunner")
    if(prop == null)
      TestRunnerType.JUNIT
    else
      TestRunnerType.withName(prop)
  }

  lazy val methodCallsNotCollect = {
    val prop = ConfigurationProperties.getProperty("methodCallsNotCollect")
    if(prop == null)
      ""
    else
      prop
  }

  // Control what variables to collect runtime env
  lazy val collectFields: Boolean = {
    val prop = ConfigurationProperties.getProperty("collectFields")
    if(prop == null)
      false
    else
      prop.toBoolean
  }

  //TODO: this should be true by default?
  lazy val collectUsedVarAtFault: Boolean = {
    val prop = ConfigurationProperties.getProperty("usedVarAtFault")
    if(prop == null)
      false
    else
      prop.toBoolean
  }

  lazy val collectLocalVisibleVars: Boolean = {
    val prop = ConfigurationProperties.getProperty("collectLocalVisibleVars")
    if(prop == null)
      true
    else
      prop.toBoolean
  }

  lazy val collectUsedVarsWholeFile: Boolean = {
    val prop = ConfigurationProperties.getProperty("usedVarsWholeFile")
    if(prop == null)
      false
    else
      prop.toBoolean
  }

  // End control run time variables

  var noCareParamTypes ={
    val prop = ConfigurationProperties.getProperty("noCareParamTypes")
    if(prop == null)
      false
    else
      prop.toBoolean
  }

  lazy val onlyFailTests = {
    val prop = ConfigurationProperties.getProperty("onlyFailTests")
    if(prop == null)
      false
    else
      prop.toBoolean
  }

  lazy val passingTests = {
    val prop = ConfigurationProperties.getProperty("passingTests")
    if(prop == null)
      null
    else
      prop.split(";")
  }

  lazy val retestSubset: Boolean = {
    val prop = ConfigurationProperties.getProperty("retestSubset")
    if(prop == null)
      false
    else
      prop.toBoolean
  }

  lazy val goldenTestClassDir: String = {
    val prop = ConfigurationProperties.getProperty("goldenTestClassDir")
    if(prop != null)
      prop
    else
      null
  }

  lazy val goldenAppClassDir: String = {
    val prop = ConfigurationProperties.getProperty("goldenAppClassDir")
    if(prop != null)
     prop
    else
      null
  }


  var seed: Long = 200

  val overfitProbability = 0.8

  var faultFile: String = ""

  var fixFile: String = ""

  def getFaultFiles(): Array[String] = {
    faultFile.split(";")
  }

  var faultFilterFile: String =""
  var failingTests: String = ""

  lazy val writeOutput2Disk: Boolean = {
    val prop = ConfigurationProperties.getProperty("writeOutput2Disk")
    if(prop != null)
      prop.toBoolean
    else true
  }

  lazy val goldenFailingTests: String = {
    val prop = ConfigurationProperties.getProperty("goldenFailingTests")
    if(prop != null)
      prop
    else null
  }

  lazy val goldenTestFolder: String = {
    val prop = ConfigurationProperties.getProperty("goldenTestFolder")
    if(prop != null)
      prop
    else null
  }

  var skipConfigValidation: Boolean = false
  lazy val frontendInstrumentOnly: Boolean = {
    val prop = ConfigurationProperties.getProperty("frontendInstrumentOnly")
    if(prop != null)
      prop.trim.toBoolean
    else false
  }

  var synthesisLevels: String = ""
  var synthesisSimplification: Boolean = false
  var spaceReduction: Boolean = true
  var synthesisTimeout: Int = 120 // in seconds
  var synthesisJar: String = ""
  var configFileX: String = "X"

  lazy val prioritizeSpace: Boolean = {
    val prop = ConfigurationProperties.getProperty("prioritizeSpace")
    if(prop == null)
      true
    else prop.toBoolean
  }

  lazy val syntaxFeature: Boolean = {
    val prop = ConfigurationProperties.getProperty("feature.syntax")
    if(prop == null)
       true
    else prop.toBoolean
  }

  lazy val semanticFeature: Boolean = {
    val prop = ConfigurationProperties.getProperty("feature.semantic")
    if(prop == null)
       true
    else prop.toBoolean
  }

  lazy val synthesisSolverBound: Int = {
    val prop = ConfigurationProperties.getProperty("synthesisSolverBound")
    if(prop == null)
      4
    else prop.toInt
  }

  lazy val synthesisEngine: S3Properties.SynthesisEngine.Engine = {
    val engine = ConfigurationProperties.getProperty("synthesisEngine")
    if(engine != null)
      S3Properties.SynthesisEngine.withName(engine)
    else S3Properties.SynthesisEngine.withName("angelixMaxSat")
  }

  lazy val solverPath: String = {
    val path = ConfigurationProperties.getProperty("solverPath")
    if(path == null)
      throw new RuntimeException("Need to specify solver path!")
    else
      path
  }

  lazy val beautifierPath: String = {
    val path = ConfigurationProperties.getProperty("beautifierPath")
    if(path == null)
      throw new RuntimeException("Need to specify beautifierPath!")
    else
      path
  }

  lazy val synthesisConfigLevel = {
    val level = ConfigurationProperties.getProperty("synthesisConfigLevel")
    if(level != null){
      level
    }else "linear"
  }

  var homeFolder: String = ""
  //var pkgInstrument: String = ""
  var sourceFolder: String = ""

  var filterFaultScore: Double = 0.5
  var faultLines: String = null
  //var root: String =  "/home/dxble/"

  var junitDeps = ""
  var jpfDeps = ""
  var jpfSearchDepth = "110" //"110"
  //var jpfMultipleErrors = true
  var jpfSymbcSolver= "choco"
  lazy val terminateSearchWhenHit: Boolean = {
    if(ConfigurationProperties.hasProperty("terminateSearchWhenHit"))
      ConfigurationProperties.getProperty("terminateSearchWhenHit").toBoolean
    else true
  }

  var jpfMinInt= -10000000
  var jpfMaxInt= 10000000
  var jpfMinDouble= -1.7976931348623157E308 //-10000000.0
  var jpfMaxDouble=  1.7976931348623157E308 //10000000.0

  lazy val jpfSearchMultipleErrors: Boolean = {
    if(ConfigurationProperties.hasProperty("jpfSearchMultipleErrors"))
      ConfigurationProperties.getProperty("jpfSearchMultipleErrors").trim.toBoolean
    else true
  }

  lazy val nhandlerDelegateUnhandledNative: Boolean = {
    if(ConfigurationProperties.hasProperty("nhandlerDelegateUnhandledNative"))
      ConfigurationProperties.getProperty("nhandlerDelegateUnhandledNative").trim.toBoolean
    else false
  }

  lazy val nhandlerDelegates: String = {
    if(ConfigurationProperties.hasProperty("nhandlerDelegates"))
      ConfigurationProperties.getProperty("nhandlerDelegates")
    else null
  }

  var dependencies: String = ""
  var localLibs: String = ""

  lazy val localLibFolders: Array[String] = {
    val prop = ConfigurationProperties.getProperty("localLibFolders")
    if(prop == null)
      Array()
    else prop.split(";")
  }

  lazy val pomDeps: ArrayBuffer[String] = {
    val prop = ConfigurationProperties.getProperty("pomDeps")
    if(prop == null)
      new ArrayBuffer[String]()
    else {
      if(depsHome == null)
        throw new RuntimeException("Need to specify pomDepsHome!")
      prop.split(";").foldLeft(new ArrayBuffer[String]()){
        (res, dep) => {
          val elems = dep.split(":")
          val e0 = elems(0).replace(".","/")
          val adep = elems.drop(1).foldLeft(depsHome+File.separator+e0){
            (res, s) => {
              res+File.separator+s
            }
          }
          res.append(adep)
          res
        }
      }
    }
  }

  lazy val gradleDeps: ArrayBuffer[String] = {
    val prop = ConfigurationProperties.getProperty("gradleDeps")
    if(prop == null)
      new ArrayBuffer[String]()
    else {
      if(depsHome == null)
        throw new RuntimeException("Need to specify pomDepsHome!")
      prop.split(";").foldLeft(new ArrayBuffer[String]()){
        (res, dep) => {
          val elems = dep.split(":")
          val adep = elems.foldLeft(depsHome){
            (res, s) => {
              res+File.separator+s
            }
          }
          res.append(adep)
          res
        }
      }
    }
  }

  lazy val depsHome: String = {
    val prop = ConfigurationProperties.getProperty("pomDepsHome")
    prop
  }

  var testFolder: String = ""
  var maxSolution: Int = 10
  lazy val testTimeout: Int = {
    val prop = ConfigurationProperties.getProperty("testTimeout")
    if(prop == null)
      25
    else prop.toInt
  }// seconds

  lazy val frontEndInstrumentation: Boolean = {
    val prop = ConfigurationProperties.getProperty("frontEndInstrumentation")
    if(prop == null)
      true
    else prop.toBoolean
  }

  var bugName: String ="m22"
  var libs = Array("/usr/lib/jvm/java-8-oracle/jre/lib/rt.jar")//Array("/usr/lib/jvm/java-1.7.0-openjdk-amd64/jre/lib/rt.jar")//

  var localizationMethod = "ochiai"
  var suspicious = 20
  var groupByScore = false
  var groupSize = 1

  lazy val combinePatchIfFail = {
    val combine = ConfigurationProperties.getProperty("allowRepairField")
    if(combine == null)
      true
    else combine.toBoolean
  }

  var assertFile = ""
  var angelixRoot = ""

  //var minedPatternFile = root+"/workspace/historicalfixv2/allLibs/workingdata/test.lg"
  var totalGraphs = 1
  val usage = """
  Usage: parser [-v] [-f file] [-s sopt] ...
  Where: -v   Run verbosely
       -f F Set input file to F
       -s S Set Show option to S
              """
  val JAVA_LANG: String = "java"

  var showme: String = ""
  var debug: Boolean = true

  val unknown = "(^-[^\\s])".r
  var language: String = JAVA_LANG

  //var defaultVariantNumber = "default" //temp1
  var appClassDir = ""
  var testClassDir = ""
  var thresholdFL = 0.005

  //**** Repairable restrictions ******
  // additionally allow: methodCalls, returns, guards
  var defectClasses: String = "assignments;if-conditions;loop-conditions"
  lazy val repairEnum: Boolean = {
    val repType = ConfigurationProperties.getProperty("repairEnum")
    if(repType != null)
      repType.toBoolean
    else false
  }
  // Note: this allow to instrument object and null literal
  lazy val repairNullness: Boolean = {
    val repType = ConfigurationProperties.getProperty("repairNullness")
    if(repType != null)
      repType.toBoolean
    else false
  }

  lazy val repairableTypes: String = {
    val repTypes = ConfigurationProperties.getProperty("repairableTypes")
    if(repTypes != null)
      repTypes
    else "int;double;bool;char;float"
  }

  lazy val notRepairableTypes: String = {
    val repTypes = ConfigurationProperties.getProperty("notRepairableTypes")
    if(repTypes != null)
      repTypes
    else ""
  }

  lazy val allowRepairField: Boolean = {
    val repField = ConfigurationProperties.getProperty("allowRepairField")
    if(repField == null)
      false
    else repField.toBoolean
  }

  lazy val notInstrumentVars: Set[String] = {
    val notInstrument = ConfigurationProperties.getProperty("notInstrumentVars")
    if(notInstrument == null)
      new immutable.HashSet[String]
    else notInstrument.split(";").toSet
  }

  lazy val methodCallRepairType: Array[S3Properties.MethodCallRepType.MCRepType] = {
    // This can be: 1. currentClass 2. visibleVars 3. usedVars
    // 1. consider all available methods in current class containing buggy line
    // 2. consider methods available in visible vars at buggy line
    // 3. consider methods available in used vars at buggy line

    val mcRepType = ConfigurationProperties.getProperty("methodCallRepairType")
    if(mcRepType != null) {
      mcRepType.split(";").map(t => S3Properties.MethodCallRepType.withName(t))
    }
    else Array()
  }

  lazy val collectObjectVar: Boolean = {
    val res = ConfigurationProperties.getProperty("collectObjectVar")
    if(res != null)
      res.toBoolean
    else methodCallRepairType.contains(MethodCallRepType.METH_IN_USED_VAR)
  }

  lazy val methodCallSuperClassLevel: Int = {
    val level = ConfigurationProperties.getProperty("methodCallSuperClassLevel")
    if(level != null) {
      level.toInt
    }
    else 0
  }

  lazy val methodCallNumberParams: Int = {
    val numberParamCanHandle = ConfigurationProperties.getProperty("methodCallNumberParams")
    if(numberParamCanHandle != null) {
      numberParamCanHandle.toInt
    }
    else 0
  }
  //**** End Repairable restrictions *****

  lazy val testsIgnored: Array[String] = getTestsIgnored()
  lazy val testsFullNameIgnored: Array[String] = getTestsFullNameIgnored
  lazy val testMethodNamePatterns: Array[String] = getTestMethodNamePatterns()
  lazy val ignorePositiveTestsFail: Boolean = {
    val ignore = ConfigurationProperties.getProperty("ignorePositiveTestsFail")
    if(ignore != null)
      ignore.toBoolean
    else false
  }

  lazy val testSetUpMethods: Array[String] = {
    val mts = ConfigurationProperties.getProperty("testSetUpMethods")
    if(mts != null)
      mts.split(";")
    else Array()
  }

  def getTestForValidation(): Array[String] = {
    val tests = ConfigurationProperties.getProperty("testForValidation")
    if(tests != null)
      tests.split(";")
    else null
  }

  lazy val test2Debug: String = {
    val prop = ConfigurationProperties.getProperty("test2Debug")
    prop
  }

  lazy val testForValidation: Array[String] = getTestForValidation()
  lazy val applyFixRetestOnly: Boolean = {
    val prop = ConfigurationProperties.getProperty("applyFixRetestOnly")
    if(prop != null)
      prop.toBoolean
    else false
  }

  lazy val patchFile: String = {
    val prop = ConfigurationProperties.getProperty("patchFile")
    if(prop != null)
      prop
    else null
  }

  lazy val inferNegativeTestsOnly: Boolean = onlyUseNegativeTestsForInference()

  lazy val inferInitialTests: Int = {
    val initialTests = ConfigurationProperties.getProperty("inferInitialTests")
    if(initialTests == null)
      2
    else initialTests.toInt
  }

  lazy val inferSpecificTests: Array[String] = {
    val initialTests = ConfigurationProperties.getProperty("inferSpecificTests")
    if(initialTests == null)
      null
    else initialTests.split(";")
  }

  lazy val inferAllTests: Boolean = {
    val initialTests = ConfigurationProperties.getProperty("inferAllTests")
    if(initialTests == null)
      false
    else initialTests.toBoolean
  }

  lazy val classPath: String = {
    val path = ConfigurationProperties.getProperty("classPath")
    if(path == null)
      ""
    else path.replace(":",";")
  }

  private def onlyUseNegativeTestsForInference(): Boolean = {
    val negInfer = ConfigurationProperties.getProperty("inferNegativeTestsOnly")
    if(negInfer != null)
      negInfer.trim.toBoolean
    else
      false
  }

  private def getTestMethodNamePatterns() ={
    val patterns = ConfigurationProperties.getProperty("testMethodNamePatterns")
    if(patterns != null)
      patterns.split(";")
    else null
  }

  // test class name, e.g., a.b.c.ClassName
  private def getTestsIgnored() = {
    val toIgnore = ConfigurationProperties.getProperty("testsIgnored")
    if(toIgnore != null){
      toIgnore.split(";")
    }else Array[String]()
  }

  // test class name and method name, e.g., a.b.c#method
  private def getTestsFullNameIgnored() = {
    val toIgnore = ConfigurationProperties.getProperty("testsFullNameIgnored")
    if(toIgnore != null){
      toIgnore.split(";")
    }else Array[String]()
  }

  private var dependenciesList: java.util.ArrayList[String] = null

  // def testClassAbsoluteDir(): String = homeFolder+ File.separator + testClassDir
  // def appClassAbsoluteDir(): String = homeFolder+ File.separator + appClassDir

  def getJars(folder: String) = {
    val foundJars = new util.ArrayList[String]()
    FileFolderUtils.walkRecString(folder,".jar", foundJars)
    foundJars
  }

  def getDependenciesList(): java.util.ArrayList[String] = {
    if(dependenciesList != null)
      return dependenciesList


    dependenciesList = dependencies.split(";").foldLeft(new java.util.ArrayList[String]()) {
      (res, dp) => {res.add(dp); res}
    }

    val jpfDepenencies= new util.ArrayList[String]()
    jpfDeps.split(";").foreach(f => jpfDepenencies.addAll(getJars(f)))
    dependenciesList.addAll(jpfDepenencies)

    val depFromLocalLibs = new util.ArrayList[String]()
    localLibs.split(";").foreach(f => depFromLocalLibs.addAll(getJars(f)))

    localLibFolders.foreach {dependenciesList.add(_)}
    dependenciesList.addAll(depFromLocalLibs)

    val depsPOM = new util.ArrayList[String]()
    pomDeps.foreach(f => depsPOM.addAll(getJars(f)))
    dependenciesList.addAll(depsPOM)

    val depsGradle = new util.ArrayList[String]()
    gradleDeps.foreach(f => depsGradle.addAll(getJars(f)))
    dependenciesList.addAll(depsGradle)

    dependenciesList.addAll(libs.foldLeft(new util.ArrayList[String]()){(res, path)=>{res.add(path);res}})
    if(classPath != "") {
      println(classPath)
      classPath.split(";").foreach(p => dependenciesList.add(p))
      //dependenciesList.add(classPath)
    }
    println("List of Dependencies From ~localLibs, libs, dependencies options~: \n"+dependenciesList)
    return dependenciesList
  }
}
