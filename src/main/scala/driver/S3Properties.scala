package driver


/**
  * Created by dxble on 9/2/16.
  */
object S3Properties {
  val SYMBOLIC_RUNTIME = "SYMBOLIC_RUNTIME"
  val ANGELIX_FOLDER = "ANGELIX_FOLDER"
  val RUNNING_TEST_NUMBER = "RUNNING_TEST_NUMBER"
  val ANGELIX_DUMP = "ANGELIX_DUMP"
  val DEBUG = "DEBUG"
  val MANGLED_METHOD_CALL_FILE = "mangledMC.txt"
  val MANGLED_NULL_CHECK_FILE = "mangledNCK.txt"
  val MANGLED_VAR_FILE = "mangledVar.txt"
  val ACTUAL_VAR_TYPES_FILE = "actualVarType.txt"

  object SynthesisLevel extends Enumeration{
    type SynthLevel = Value
    val ALTENATIVES = Value("alternatives")
    val VARIABLES = Value("variables")
    val BASIC_ARITHMETIC = Value("basic-arithmetic") //
    val BASIC_INEQUALITIES= Value("basic-inequalities")
    val BASIC_EQUALITIES= Value("basic-equalities")
    val BASIC_LOGIC = Value("basic-logic")
    val INTEGER_CONSTANTS = Value("integer-constants")
    val BOOLEAN_CONSTANTS = Value("boolean-constants")
  }

  object SynthesisEngine extends Enumeration{
    type Engine = Value
    val SYNTHESIS_ANGELIX_MAXSAT = Value("angelixMaxSat")
    val SYNTHESIS_META = Value("Meta")
    val SYNTHESIS_CVC4 = Value("cvc4")
    val SYNTHESIS_ENUM = Value("enum")
    val OTHER = Value("other")
  }

  object MethodCallRepType extends Enumeration{
    type MCRepType = Value
    val ENCODING_ONLY = Value("encodingOnly")
    val CURRENT_CLASS = Value("currentClass")
    val USED_METHOD_CALLS = Value("usedMethodCalls")
    val USED_METH_ELSE_WHERE = Value("usedMethElseWhere")
    // used method calls, replace only one param of it
    val USED_METH_1_PARAM = Value("usedMeth1Param")
    val METH_IN_USED_VAR = Value("methInUsedVar")
  }

  object TestRunnerType extends Enumeration {
    type TRType = Value
    val JUNIT = Value("junit")
    val TESTNG = Value("testng")
  }

  object VariableKind extends Enumeration{
    type VarKind = Value
    val NONSTATIC_FIELD = Value("ENCODEVAR_NonStaticField")
    val STATIC_FIELD = Value("ENCODEVAR_StaticField")
    val METHOLD_VAR = Value("ENCODEVAR_MethodVar") // local vars
    val USED_VAR= Value("ENCODEVAR_UsedVar")
    val USED_VAR_AVAIL = Value("ENCODEVAR_UsedVarAvail")
  }
}
