package frontend

import java.io.File
import java.net.URL
import driver.Options

/**
  * Created by dxble on 9/28/16.
  */
object Golden {

  def isGoldenSetUp(): Boolean = {
    Options.goldenAppClassDir != null && Options.goldenTestClassDir != null
  }

  def classPaths() = {
    val bytecodeClasses: String = Options.goldenAppClassDir
    val bytecodeTestClasses: String = Options.goldenTestClassDir
    val goldenApp: File = new File(bytecodeClasses)
    val goldenTest: File = new File(bytecodeTestClasses)
    var bc: Array[URL] = null
    val originalURL: Array[URL] = utils.FileFolderUtils.getURLforInstrumented()
    bc = utils.FileFolderUtils.redefineURL(goldenTest, originalURL)
    bc = utils.FileFolderUtils.redefineURL(goldenApp, bc)
    bc
  }
}
