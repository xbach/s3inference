package localization

import driver.Options
import org.eclipse.jdt.core.dom.{ASTNode, CompilationUnit}
import parser.javaparser.JavaParser
import testrunner.{NegativeTest, PositiveTest}
import utils.ASTUtils

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Created by dxble on 8/29/16.
  */
trait LocalizationFormula{
  def formula(execPos: Int, execNeg: Int, totalPos: Int, totalNeg: Int): Double
}
case class Ochiai() extends LocalizationFormula{
  override def formula(execPos: Int, execNeg: Int, totalPos: Int, totalNeg: Int): Double = {
    if(totalNeg == 0)
      throw new RuntimeException("Number of Negative Tests is Zero!")

    if(execPos + execNeg == 0)
      return 0.0
    return execNeg / Math.sqrt(totalNeg * (execNeg + execPos))
  }
}

class Localization(negTests: ArrayBuffer[NegativeTest], posTests: ArrayBuffer[PositiveTest]) {
  lazy val positiveTraces: ArrayBuffer[Trace] = positiveTraces(posTests)
  lazy val negativeTraces: ArrayBuffer[Trace] = negativeTraces(negTests)

  def positiveTraces(posTests: ArrayBuffer[PositiveTest]): ArrayBuffer[Trace] = {
    //if(Options.faultLines != null)
    //  return null
    val positiveTraces = posTests.foldLeft(new ArrayBuffer[Trace]){
      (res, test) => {
        res.append(Trace(Trace.parseTrace(test),test))
        res
      }
    }
    return positiveTraces
  }

  def negativeTraces(negTests: ArrayBuffer[NegativeTest]): ArrayBuffer[Trace] = {
    //if(Options.faultLines != null)
    //  return null

    val negativeTraces = negTests.foldLeft(new ArrayBuffer[Trace]){
      (res, test) => {
        res.append(Trace(Trace.parseTrace(test),test))
        res
      }
    }
    return negativeTraces
  }

  def doLocalization(): ArrayBuffer[ArrayBuffer[FaultIdentifier]] ={
    if(Options.faultLines != null)
      doLocalizationWithTrace(null, null)
    else{
      doLocalizationWithTrace(positiveTraces, negativeTraces)
    }
  }

  def doLocalizationWithTrace(posTraces: ArrayBuffer[Trace], negTraces: ArrayBuffer[Trace]): ArrayBuffer[ArrayBuffer[FaultIdentifier]] ={

    // If faultLines are predefined in config, we take only those lines and return them
    // as the result of the function doLocalization
    if(Options.faultLines != null){
      // This is predefined fault lines, if exist. Predefined faults are then grouped
      // by group size.
      val predFaultLines = Options.faultLines.split(";").sliding(Options.groupSize)

      return predFaultLines.foldLeft(new ArrayBuffer[ArrayBuffer[FaultIdentifier]]())((res, faultGroup) => {
        val eachGroup = faultGroup.foldLeft(new ArrayBuffer[FaultIdentifier]){
          (gr, fault) => {
            val faultIdentifier = new FaultIdentifier(fault.split(" "))

            // getFileName takes directly from Options.faultFile. For now we only consider one file
            val ast: CompilationUnit = JavaParser.getCompilationUnit(faultIdentifier.getFileName)
            val faultNode: ASTNode = ASTUtils.findNode(ast, faultIdentifier)
            faultIdentifier.setJavaNode(faultNode)
            gr.append(faultIdentifier)
            gr
          }
        }
        res.append(eachGroup)
        res
      })
    }

    // If fault are not predefined, we do localization ourselves
    val all = posTraces.foldLeft(new mutable.HashSet[FaultIdentifier]()){
      (res, trace) => {
        trace.traceList.foreach(s => res.add(s))
        res
      }
    }

    negTraces.foldLeft(new mutable.HashSet[FaultIdentifier]()){
      (res, trace) => {
        trace.traceList.foreach(s => res.add(s))
        res
      }
    }.foreach(s => all.add(s))

    val (executedPositive, executedNegative) = all.foldLeft((new mutable.HashMap[FaultIdentifier, Int](), new mutable.HashMap[FaultIdentifier, Int]())){
      (res, eachFI) => {
        res._1 += eachFI -> 0
        res._2 += eachFI -> 0
        res
      }
    }

    posTraces.foreach(p => {
      val executed = p.traceList.toSet
      executed.foreach(ep => {
        val count = executedPositive.getOrElse(ep, 0)
        executedPositive.update(ep, count + 1)
      })
    })

    negTraces.foreach(n => {
      val executed = n.traceList.toSet
      executed.foreach(en => {
        val count = executedNegative.getOrElse(en, 0)
        executedNegative.update(en, count + 1)
      })
    })

    val localizationMethod = if(Options.localizationMethod.compareTo("ochiai") == 0) new Ochiai() else{
      throw new RuntimeException("Not supported localization method: "+Options.localizationMethod)
    }

    val withScore = all.foldLeft(new ArrayBuffer[(FaultIdentifier, Double)]()){
      (res, eachFI) => {
        val score = localizationMethod.formula(executedPositive.getOrElse(eachFI, 0), executedNegative.getOrElse(eachFI, 0), posTraces.size, negTraces.size)
        res.append((eachFI, score))
        res
      }
    }

    var top: ArrayBuffer[(FaultIdentifier, Double)] = null
    def Desc[T : Ordering] = implicitly[Ordering[T]].reverse
    if(Options.groupByScore) {
      top = withScore.sortBy(_._2)(Desc).take(Options.suspicious)
    }else{// Group by location (line)
      top = withScore.sortBy(_._1.iden(0))
    }

    val endRange = Math.ceil(Options.suspicious/Options.groupSize)
    if(top.length == 0)
      return null
    val groupsWithScore = new ArrayBuffer[(ArrayBuffer[FaultIdentifier], Double)]()

    // TODO: see later when group consider even/odd
    var index = 0
    for (i <- 0 to endRange.toInt - 1 if (index < top.size)){
      val group = new ArrayBuffer[FaultIdentifier]()
      var totalScore = 0.0
      for (j <- 0 to Options.groupSize - 1) {
        if(index<top.size) {
          val (fi, score) = top(index)
          group.append(fi)
          index += 1
          totalScore += score
        }
      }
      if(group.size > 0)
        groupsWithScore.append((group, totalScore/group.size))
    }

    val sortedGroup = groupsWithScore.sortBy(_._2).foldLeft(new ArrayBuffer[ArrayBuffer[FaultIdentifier]]){
      (res, group) => {
        group._1.foreach(fault => {
          val ast: CompilationUnit = JavaParser.getCompilationUnit(fault.getFileName)
          val faultNode: ASTNode = ASTUtils.findNode(ast, fault)
          //System.out.println("Faulty JavaNode: " + faultNode)
          fault.setJavaNode(faultNode)
        })
        res.append(group._1)
        res
      }
    }

    return sortedGroup
  }
}
