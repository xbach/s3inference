package testrunner

import java.io.File
import java.net.URL

import driver.Options
import org.apache.log4j.Logger
import org.eclipse.jdt.core.dom.{CompilationUnit, Modifier, TypeDeclaration}
import parser.javaparser.JavaParser
import utils.FileFolderUtils

import scala.collection.mutable.ArrayBuffer

/**
 * Created by dxble on 8/2/15.
 */
import me.tongfei.progressbar.ProgressBar

abstract class AbstractTestInvoker {
  var TEST_INDEX = 1
  val logger = Logger.getLogger(this.getClass)

  def invokeTest(testName: TestCase[Any], withTracing: Boolean, withDumping: Boolean, pb: ProgressBar, classPaths: Array[URL]): (Boolean, Boolean)

  def findAllTestsInFolder(folder: String, extension: String = ".java"): ArrayBuffer[String] ={
    import scala.collection.JavaConversions._
    val allTestsName= mylib.Lib.search4FilesContainName(new File(folder), "Test")
    val testFullNames=allTestsName.foldLeft(new ArrayBuffer[String]()) {
      case (testFullName, test) => {
        if (!TestCase.isAbstractTestFile(test,extension) && TestCase.isTestFile(test, extension)) {
          //testName.append(test.getName.split("\\.")(0)) // strip the .java
          testFullName.append(test.getCanonicalPath)
        }
        testFullName
      }
    }
    //all.map(test => println(test.getCanonicalPath))
    //testFullNames.map(test => println(test))
    testFullNames
  }

  def positiveTestsName(folder: String, negTests: ArrayBuffer[NegativeTest], ext: String = ".java"): ArrayBuffer[PositiveTest]={
    if(Options.onlyFailTests)
      return new ArrayBuffer[PositiveTest]()

    val allTestFullNames = findAllTestsInFolder(folder, ext)
    allTestFullNames.foldLeft(new ArrayBuffer[PositiveTest]()) { (res, testFullPath) => {
        val relativeTestPath = FileFolderUtils.relativePath(folder,testFullPath).split("\\.")(0) // strip the extension such as .java
        val testName = FileFolderUtils.path2Package(relativeTestPath)
        if((Options.passingTests != null && Options.passingTests.contains(testName)) || Options.passingTests ==  null) {
          val (cu, _) = JavaParser.parseOneFile(testFullPath, Options.testFolder, testName)
          val tp = cu.types().get(0).asInstanceOf[TypeDeclaration]
          // Make sure the Test class is not interface or abstract
          if (!tp.isInterface && !Modifier.isAbstract(tp.getModifiers)) {
            val methods = tp.getMethods
            methods.foreach(m => {
              val fullTestNameMethod = testName + "#" + m.getName
              if (TestCase.isValidTestMethod(m, testName, fullTestNameMethod) && m.getBody.statements().size() > 0
                && !negTests.exists { neg: NegativeTest => neg.sameTestName(fullTestNameMethod) }) {
                val index = TEST_INDEX
                TEST_INDEX += 1
                val ptest = new PositiveTest(fullTestNameMethod, index)
                if (canHandleConstructor(cu, ptest)) {
                  if(hasSetUpMethod(cu))
                    ptest.setHasSetUp(true)
                  logger.debug("Passing test: " + ptest)
                  res.append(ptest)
                }
              }
            })
          }
        }
        res
      }
    }
  }

  private def canHandleConstructor(cu: CompilationUnit, test: TestCase[Any]): Boolean = {
    val constructors = cu.types().get(0).asInstanceOf[TypeDeclaration].getMethods.filter(p => p.isConstructor)
    if(constructors.size >= 2){
      logger.warn("Test contains two constructors! Thus we do not know yet how to instrument test driver later...")
      logger.warn("Excluding test: "+test)
      return false
      //throw new RuntimeException("Test contains two instructors! Thus we do not know yet how to instrument test driver later...")
    }else if(constructors.size == 1){
      if(constructors(0).parameters().size() > 1) {
        logger.warn("Test constructor has more than one arg! Thus we do not know yet how to instrument test driver later...")
        logger.warn("Excluding test: "+test)
        return false
        //throw new RuntimeException("Test constructor has different than one arg! Thus we do not know yet how to instrument test driver later...")
      }
      else {
        // We assume the constructor has no arg or one arg, which is class name of type string
        test.setHasConstructor(true)
        if(constructors(0).parameters().size() == 1) {
          try {
          val pt = constructors(0).resolveBinding().getParameterTypes
          if (!pt(0).toString.contains("String")) {
            logger.warn("Test constructor has an argument which is not of type String.")
            logger.warn("Excluding test: " + test)
            return false
          }
          }catch {
            case e: Exception => {
            println("debug")
            }
          }
          test.setNumArgs(1)
        }else test.setNumArgs(0)
      }
    }else{
      test.setHasConstructor(false)
    }
    return true
  }

  // TODO: this may need recursive
  private def hasSetUpMethod(cu: CompilationUnit): Boolean = {
    val currentClass = cu.types().get(0).asInstanceOf[TypeDeclaration]
    val setupMethod = currentClass.getMethods.filter(p => !p.isConstructor && p.getName.getIdentifier.compareTo("setUp") == 0)
    if(setupMethod.length == 1)
      return true
    else if(setupMethod.length == 0){
      if(currentClass.getSuperclassType == null)
        return false

      try {
        val methods = currentClass.getSuperclassType.resolveBinding().getDeclaredMethods.filter(p => Modifier.isPublic(p.getModifiers) && !p.isConstructor && p.getName.compareTo("setUp") == 0)
        if (methods.length == 1)
          return true
      }catch {
        case e: Exception => {
          logger.warn("Should debug super class of test...")
        }
      }
    }
    return false
  }

  def negativeTestName(): ArrayBuffer[NegativeTest] ={
    val failingTests = Options.failingTests.split(";")
    failingTests.foldLeft(new ArrayBuffer[NegativeTest]){
      (res, test) => {
        val index=TEST_INDEX
        TEST_INDEX += 1
        val ntest = new NegativeTest(test,index)
        val ntestPath = ntest.classFullName.replace(".", File.separator)
        val fullPath = Options.testFolder+File.separator+ntestPath+".java"
        val (cu, _) = JavaParser.parseOneFile(fullPath, Options.testFolder, ntest.classFullName)
        if(canHandleConstructor(cu, ntest)) {
          if(hasSetUpMethod(cu))
            ntest.setHasSetUp(true)
          logger.debug("Failing Test: " + ntest)
          res.append(ntest)
        }
        res
      }
    }
  }

  def goldenNegativeTestName(): ArrayBuffer[NegativeTest] ={
    var goldenIndex = 1
    if(Options.goldenFailingTests == null)
      throw new RuntimeException("goldenFailingTests is not set, but still, it gets called!")

    val failingTests = Options.goldenFailingTests.split(";")
    failingTests.foldLeft(new ArrayBuffer[NegativeTest]){
      (res, test) => {
        val index=goldenIndex
        goldenIndex += 1
        val ntest = new NegativeTest(test,index)
        val ntestPath = ntest.classFullName.replace(".", File.separator)
        val fullPath = Options.goldenTestFolder+File.separator+ntestPath+".java"
        val (cu, _) = JavaParser.parseOneFile(fullPath, Options.goldenTestFolder, ntest.classFullName)
        if(canHandleConstructor(cu, ntest)) {
          logger.debug("Failing Test: " + ntest)
          res.append(ntest)
        }
        res
      }
    }
  }
}
