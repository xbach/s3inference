package testrunner

import java.io.File
import java.net.{MalformedURLException, URL}

import driver.S3Properties.TestRunnerType
import driver.{Options, S3Properties}
import frontend.FrontEndInstrumentVisitor
import junithandler.{JUnitExecutorProcess, TestResult}
import localization.Trace
import org.apache.log4j.Logger
import me.tongfei.progressbar.ProgressBar

import scala.collection.mutable.ArrayBuffer

/**
 * Created by xuanbach32bit on 8/27/15.
 */

class JUnitTestInvoker() extends AbstractTestInvoker{

  var negTests: ArrayBuffer[NegativeTest] = null
  var posTests: ArrayBuffer[PositiveTest] = null

  def init(): Unit ={
    negTests = negativeTestName()
    posTests = positiveTestsName(Options.testFolder, negTests)
  }

  init()

  // return true if all tests pass. Otherwise, return false and list of failing tests
  def invokeAllTests(withTracing: Boolean, withDumping: Boolean, configValidation: Boolean, classPaths: Array[URL]) : (Boolean, Array[TestCase[Any]]) = {
    // Note that we do not dump when running negative tests.
    // We already dump expected output for negative tests using assert file.
    val totalTests = negTests.size + posTests.size
    //val pb = new ProgressBar("Test", totalTests)
    //pb.start(); // the progress bar starts timing
    //pb.setExtraMessage("Running tests")
    val pb = null
    val (negPassAll,negFail) = invokeTests(withTracing, false, configValidation, negTests, pb, classPaths)
    val (posPassAll,posFail) = invokeTests(withTracing, withDumping, configValidation, posTests, pb, classPaths)
    //pb.stop()
    return (negPassAll && posPassAll, negFail ++ posFail)
  }

  def invokeTests(withTracing: Boolean, withDumping: Boolean, configValidation: Boolean,
                  tests: ArrayBuffer[_<:TestCase[Any]], pb: ProgressBar, classPaths: Array[URL]) : (Boolean, Array[TestCase[Any]]) = {
    var allTestsPass = true
    val failedTests = tests.foldLeft(Array[TestCase[Any]]())((res, t) => {
      val (pass, _) = invokeTest(t, withTracing, withDumping, pb, classPaths)
      if(configValidation)
        if(pass && t.isInstanceOf[NegativeTest])
          throw new RuntimeException("Negative Test pass: " + t + " ==> check your failingTests configuration!")
        else if(!pass && t.isInstanceOf[PositiveTest])
          throw new RuntimeException("Positive Test fail: " + t + " ==> check your failingTests configuration!")

      if (!pass) {
        allTestsPass = false
        res :+ t
      } else {
          res
      }
    })
    (allTestsPass, failedTests)
  }

  def findTestWithName(testName: String): TestCase[Any] = {
    negTests.find(n => n.getFullNameWithMethod().compareTo(testName) == 0).getOrElse(posTests.find(p => p.getFullNameWithMethod().compareTo(testName) ==0).getOrElse(null))
  }

  def invokeOneTestFresh(test: TestCase[Any], withTracing: Boolean, withDumping: Boolean,
                         classPaths: Array[URL]): (Boolean, Boolean) ={
    val pb = new ProgressBar("Test", 1)
    pb.start(); // the progress bar starts timing
    pb.setExtraMessage("Running tests")
    val temp = invokeTest(test, withTracing, withDumping, pb, classPaths)
    pb.stop()
    temp
  }

  def invokeTestsFresh(withTracing: Boolean, withDumping: Boolean, configValidation: Boolean,
                  tests: ArrayBuffer[_<:TestCase[Any]], classPaths: Array[URL]) : (Boolean, Array[TestCase[Any]]) = {
    val pb = new ProgressBar("Test", tests.size)
    pb.start(); // the progress bar starts timing
    pb.setExtraMessage("Running tests")
    val temp = invokeTests(withTracing, withDumping, configValidation, tests, pb, classPaths)
    pb.stop()
    temp
  }

  override def invokeTest(test: TestCase[Any], withTracing: Boolean, withDumping: Boolean,
                          pb: ProgressBar, classPaths: Array[URL]): (Boolean, Boolean) ={
    var testRes : TestResult = null
    val dumpProperty = if(withDumping) S3Properties.ANGELIX_DUMP+"=true" else S3Properties.ANGELIX_DUMP+"=false"

    if(withTracing) {
      val traceFolderPath = Trace.getTraceFolderPath()
      val traceFolder = new File(traceFolderPath)
      if (!traceFolder.exists())
        traceFolder.mkdir()
      val traceFolderOfTest = traceFolderPath + File.separator + test.getId()
      testRes = validate(test, Array("ANGELIX_TRACE=" + traceFolderOfTest, S3Properties.RUNNING_TEST_NUMBER +"="+test.getId().toString, dumpProperty,S3Properties.ANGELIX_FOLDER +"="+System.getProperty(S3Properties.ANGELIX_FOLDER),"DEBUG="+Options.debug), classPaths)
    }else{
      testRes = validate(test, Array(S3Properties.RUNNING_TEST_NUMBER +"="+test.getId().toString, dumpProperty, S3Properties.ANGELIX_FOLDER +"="+System.getProperty(S3Properties.ANGELIX_FOLDER),"DEBUG="+Options.debug), classPaths)
    }
    if(pb != null)
      pb.step()
    if(testRes != null)
    {
      //println(testRes)
      (testRes.wasSuccessful(), false) // compilation error false because a variant gone through validation step is a compilable one already
    }else
      (false,false)
  }

  private def validate (test: TestCase[Any], properties: Array[String], classPaths: Array[URL]): TestResult = {
    try {
      val p: JUnitExecutorProcess = new JUnitExecutorProcess
      /*classOf[SingleJUnitTestRunner].getCanonicalName*/
      logger.debug("Run test: "+test)
      properties.foreach(p=>
        logger.debug("Props: "+p)
      )
      val runner = if(Options.testRunner == TestRunnerType.JUNIT) "junithandler.SingleJUnitTestRunner" else if(Options.testRunner == TestRunnerType.TESTNG) "junithandler.SingleTestNGTestRunner" else throw new RuntimeException("Not rupported test runer type!")
      val trfailing: TestResult = p.execute(classPaths,test.getFullNameWithMethod(), runner, Options.testTimeout, properties)
      return trfailing
    }
    catch {
      case e: MalformedURLException => {
        e.printStackTrace
        throw e
      }
    }
  }

}
