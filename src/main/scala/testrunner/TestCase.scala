package testrunner

import java.io.File

import driver.Options
import org.eclipse.jdt.core.dom.{Annotation, Dimension, MethodDeclaration, Modifier}


/**
 * Created by xuanbach32bit on 4/25/15.
 */
abstract class TestCase[+T]{
  lazy val packageName: String = getPackageName()
  lazy val classFullName: String = getClassFullyQualifiedName()
  lazy val className: String = classFullName.split("\\.").last
  lazy val methodName: String = getMethodName()
  var hasConstructor: Boolean = false
  var numArgs : Int = 0
  var hasSetUp: Boolean = false

  def setHasConstructor(has: Boolean) = hasConstructor = has
  def setNumArgs(num: Int) = numArgs = num
  def setHasSetUp(has: Boolean) = hasSetUp = has

  private def getPackageName(): String = {
    if(classFullName.contains(".")){
      val pkg = classFullName.split("\\.")
      val pkgName = pkg.take(pkg.length - 1).foldLeft(""){(res, i)=> {
        if(res == "")
          i
        else
          res+"."+i
      }}
      return pkgName
    }else{
      return null
    }
  }

  //def setPackageName(pkg: String) = packageName = pkg
  def getFullNameWithMethod():String

  private def getClassFullyQualifiedName(): String = {
    getFullNameWithMethod().split("#")(0)
  }

  private def getMethodName(): String ={
    getFullNameWithMethod().split("#")(1)
  }

  def getId(): Int

  protected def sameTestName(that: String): Boolean ={
    val classNameMethodName = getFullNameWithMethod().split("#")
    val thatClassNameMethodName = that.split("#")
    return classNameMethodName(0).compareTo(thatClassNameMethodName(0)) == 0 && classNameMethodName(1).compareTo(thatClassNameMethodName(1)) == 0
  }

  override def equals(obj: Any): Boolean ={
    if(obj.isInstanceOf[TestCase[Any]]){
      val objTest = obj.asInstanceOf[TestCase[Any]]
      val currentNoNumberSign = getFullNameWithMethod()
      val objNoNumberSign = objTest.getFullNameWithMethod()
      if(currentNoNumberSign.compareTo(objNoNumberSign) == 0)
        return true
      else
        return false
    }else
      return false
  }
}
case class PositiveTest(name:String, id: Int) extends TestCase[Any]{
  def getFullNameWithMethod():String=name
  def getId(): Int=id
  override def sameTestName(name: String) = super.sameTestName(name)
}
case class NegativeTest(name:String, id: Int) extends TestCase[Any]{
  def getFullNameWithMethod():String=name
  def getId(): Int=id
  override def sameTestName(name: String) = super.sameTestName(name)
}

object TestCase{
  def isNotTestFile(f: File, ext: String =".java"): Boolean ={
    return !f.getName.endsWith("TestPermutations"+ext) &&
      !f.getName.endsWith("Test"+ext) && !f.getName.endsWith("TestCase"+ext) &&
      !f.getName.endsWith("AbstractTest"+ext) && f.getName.endsWith(ext) && !f.getName.endsWith("~") &&
      !f.getName.endsWith("Tests"+ext) && !f.getName.startsWith("Test")
  }

  def isTestFile(f: File, ext: String =".java"): Boolean={
    return f.getName.endsWith("TestPermutations"+ext) ||
      f.getName.endsWith("Test"+ext) || f.getName.endsWith("TestCase"+ext) ||
      f.getName.endsWith("AbstractTest"+ext) || f.getName.endsWith("Tests"+ext) ||
      (f.getName.startsWith("Test") && f.getName.endsWith(ext))
  }

  def isAbstractTestFile(f: File, ext : String = ".java"): Boolean={
    return f.getName.contains("AbstractTest"+ext)
  }

  def followTestMethodConvention(methodName: String): Boolean = {
    if(Options.testMethodNamePatterns != null)
      Options.testMethodNamePatterns.exists(p => methodName.contains(p))
    else
      true
  }

  //TODO: to later see how to implement this properly
  def containTestAnnotation(m: MethodDeclaration): Boolean = {
    import scala.collection.JavaConversions._
    m.extraDimensions().exists(et =>
      et.asInstanceOf[Dimension].annotations().exists(anno =>
        anno.asInstanceOf[Annotation].getTypeName.getFullyQualifiedName.compareTo("org.junit.Test")==0))
  }

  def isValidTestMethod(m: MethodDeclaration, testClassName: String, fullTestNameMethod: String): Boolean = {
    val followConvention = TestCase.followTestMethodConvention(m.getName.toString)
    // make sure a test method is public and not static
    val isPublicMethod = !m.isConstructor && Modifier.isPublic(m.getModifiers) && !Modifier.isStatic(m.getModifiers)
    return (!Options.testsIgnored.contains(testClassName)
           && !Options.testsFullNameIgnored.contains(fullTestNameMethod)
           && followConvention && isPublicMethod)
  }
}