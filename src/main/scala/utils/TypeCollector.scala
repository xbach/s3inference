package utils

/**
  * Created by dxble on 8/30/16.
  */
import org.eclipse.jdt.core.dom._
import java.util.Set


class TypeCollector(typeSet: Set[String]) extends ASTVisitor {

  override def visit(node: SimpleName): Boolean = {
    val nodeBinding = node.resolveBinding()
    val typeBinding = node.resolveTypeBinding()
    // && !typeBinding.isPrimitive
    if (nodeBinding != null && typeBinding != null && nodeBinding.isInstanceOf[IVariableBinding]) {
      val nodeType = typeBinding.getQualifiedName
      typeSet.add(nodeType)
    }
    super.visit(node)
  }
}
