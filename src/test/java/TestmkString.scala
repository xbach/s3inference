import scala.collection.mutable

/**
  * Created by dxble on 2/25/17.
  */
object TestmkString {
  def main(args: Array[String]): Unit = {
    val x = new mutable.HashMap[String, String] ()
    x += "a" -> "int"
    x += "b" -> "char"
    println(x.mkString("\n").replace(" -> ",","))
  }
}
